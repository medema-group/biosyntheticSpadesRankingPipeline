#!/usr/bin/env python

"""
PI: Marnix Medema               marnix.medema@wur.nl

Developers:
Jorge Navarro                   jorge.navarromunoz@wur.nl
Emmanuel (Emzo) de los Santos   E.De-Los-Santos@warwick.ac.uk
Marley Yeong                    marleyyeong@live.nl
Vittorio Tracanna               vittorio.tracanna@wur.nl


Dependencies: hmmer, biopython, (mafft), munkres.py

See more info on
https://git.wageningenur.nl/medema-group/biosyntheticSpadesRankingPipeline

# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.
"""

import cPickle as pickle  # for storing and retrieving dictionaries
import os
from itertools import combinations
from collections import defaultdict
from multiprocessing import Pool, cpu_count
from argparse import ArgumentParser
from Bio import pairwise2
from Bio.SubsMat.MatrixInfo import pam250 as scoring_matrix

from functions import *
from munkres import Munkres
from ArrowerSVG import *

from array import array


def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

def generate_network(cluster_pairs, cores):
    """Distributes the distance calculation part
    cluster_pairs is a list of triads (cluster1_index, cluster2_index, BGC class)
    """

    pool = Pool(cores, maxtasksperchild=100)

    # Assigns the data to the different workers and pools the results back into
    # the network_matrix variable
    print (cluster_pairs)
    network_matrix = map(generate_dist_matrix, cluster_pairs)

    # --- Serialized version of distance calculation ---
    # For the time being, use this if you have memory issues
    # network_matrix = []
    # for pair in cluster_pairs:
    # network_matrix.append(generate_dist_matrix(pair))

    return network_matrix

def generate_dist_matrix(parms):
    """Unpack data to actually launch cluster_distance for one pair of BGCs"""
    cluster1Idx, cluster2Idx, bgcClassIdx = map(int, parms)
    cluster1 = clusterNames[cluster1Idx]
    cluster2 = clusterNames[cluster2Idx]
    bgc_class = bgcClassNames[bgcClassIdx]

    try:
        domain_list_A = DomainList[cluster1]
        domain_list_B = DomainList[cluster2]
    except KeyError:
        print(" Warning: domain list for " + cluster1 + " or " + cluster2 + " was not found. Extracting from pfs files")

        cluster_file1 = os.path.join(output_folder, cluster1 + ".pfs")
        cluster_file2 = os.path.join(db_folder, cluster2 + ".pfs")

        domain_list_A = get_domain_list(cluster_file1)
        domain_list_B = get_domain_list(cluster_file2)

    # this really shouldn't happen if we've filtered domain-less gene clusters already
    if len(domain_list_A) == 0 or len(domain_list_B) == 0:
        print("   Warning: Regarding distance between clusters " + cluster1 + " and " + cluster2 + ":")
        if len(domain_list_A) == 0 and len(domain_list_B) == 0:
            print("   None have identified domains. Distance cannot be calculated")
        elif (domain_list_A) == 0:
            print("   Cluster " + cluster1 + " has no identified domains. Distance set to 1")
        else:
            print("   Cluster " + cluster2 + " has no identified domains. Distance set to 1")

        # last two values (S, Sa) should really be zero but this could give rise to errors when parsing
        # the network file (unless we catched the case S = Sa = 0

        # cluster1Idx, cluster2Idx, bgcClassIdx, distance, jaccard, DSS, AI, rDSSNa, rDSSa, S, Sa
        return array('f', [cluster1Idx, cluster2Idx, bgcClassIdx, 1, 0, 0, 0, 0, 0, 1, 1])

    dist, jaccard, dss, ai, rDSSna, rDSS, S, Sa = cluster_distance(cluster1, cluster2,
                                                                   domain_list_A, domain_list_B,
                                                                   bgc_class)  # sequence dist

    network_row = array('f',
                        [cluster1Idx, cluster2Idx, bgcClassIdx, dist, (1 - dist) ** 2, jaccard, dss, ai, rDSSna, rDSS,
                         S, Sa])

    return network_row

def cluster_distance(A, B, a_domlist, b_domlist, bgc_class):
    """Compare two clusters using information on their domains, and the 
    sequences of the domains"""

    Jaccardw, DSSw, AIw, anchorboost = bgc_class_weight[bgc_class]
    temp_domain_fastas = {}
    A_domlist = a_domlist[:]
    B_domlist = b_domlist[:]

    setA = set(A_domlist)
    setB = set(B_domlist)
    intersect = setA.intersection(setB)

    S = 0
    S_anchor = 0

    # Detect totally unrelated pairs from the beginning
    if len(intersect) == 0:
        # Count total number of anchor and non-anchor domain to report in the
        # network file. Apart from that, these BGCs are totally unrelated.
        for domain in setA:
            # This is a bit of a hack. If pfam domain ids ever change in size
            # we'd be in trouble. The previous approach was to .split(".")[0]
            # but it's more costly
            if domain[:7] in anchor_domains:
                S_anchor += len(BGCs[A][domain])
            else:
                S += len(BGCs[A][domain])

        for domain in setB:
            if domain[:7] in anchor_domains:
                S_anchor += len(BGCs[B][domain])
            else:
                S += len(BGCs[B][domain])

        return 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, S, S_anchor

    # define the subset of domain sequence tags to include in
    # the DSS calculation. This is done for every domain.
    A_domain_sequence_slice_bottom = defaultdict(int)
    A_domain_sequence_slice_top = defaultdict(int)
    B_domain_sequence_slice_bottom = defaultdict(int)
    B_domain_sequence_slice_top = defaultdict(int)

    # In local mode, try to align the shorter BGC ("BGC-fragment") to the
    # best matching slice of the larger BGC


    # initialize domain sequence slices
    for domain in setA:
        A_domain_sequence_slice_bottom[domain] = 0
        A_domain_sequence_slice_top[domain] = len(BGCs[A][domain])

    for domain in setB:
        B_domain_sequence_slice_bottom[domain] = 0
        B_domain_sequence_slice_top[domain] = len(BGCs[B][domain])

    # JACCARD INDEX
    Jaccard = len(intersect) / float(len(setA) + len(setB) - len(intersect))

    # DSS INDEX
    # domain_difference: Difference in sequence per domain. If one cluster does
    # not have a domain at all, but the other does, this is a (complete)
    # difference in sequence 1. If both clusters contain the domain once, and
    # the sequence is the same, there is a seq diff of 0.
    # S: Max occurence of each domain
    domain_difference_anchor, S_anchor = 0, 0
    domain_difference, S = 0, 0

    not_intersect = setA.symmetric_difference(setB)

    # Case 1
    # no need to look at seq identity, since these domains are unshared
    for unshared_domain in not_intersect:
        # for each occurence of an unshared domain do domain_difference += count
        # of domain and S += count of domain
        unshared_occurrences = []

        try:
            unshared_occurrences = BGCs[A][unshared_domain]
        except KeyError:
            unshared_occurrences = BGCs[B][unshared_domain]

        # don't look at domain version, hence the split
        if unshared_domain[:7] in anchor_domains:
            domain_difference_anchor += len(unshared_occurrences)
        else:
            domain_difference += len(unshared_occurrences)

    S = domain_difference  # can be done because it's the first use of these
    S_anchor = domain_difference_anchor

    # Cases 2 and 3 (now merged)
    missing_aligned_domain_files = []

    for shared_domain in intersect:

        specific_domain_list_A = BGCs[A][shared_domain]
        specific_domain_list_B = BGCs[B][shared_domain]

        num_copies_a = A_domain_sequence_slice_top[shared_domain] - A_domain_sequence_slice_bottom[shared_domain]
        num_copies_b = B_domain_sequence_slice_top[shared_domain] - B_domain_sequence_slice_bottom[shared_domain]

        temp_domain_fastas.clear()

        accumulated_distance = 0

        # Fill distance matrix between domain's A and B versions
        DistanceMatrix = [[1 for col in range(num_copies_b)] for row in range(num_copies_a)]

        for domsa in range(num_copies_a):
            for domsb in range(num_copies_b):
                sequence_tag_a = specific_domain_list_A[domsa + A_domain_sequence_slice_bottom[shared_domain]]
                sequence_tag_b = specific_domain_list_B[domsb + B_domain_sequence_slice_bottom[shared_domain]]

                seq_length = 0
                matches = 0
                gaps = 0
                try:
                    aligned_seqA = AlignedDomainSequences[sequence_tag_a]
                    aligned_seqB = AlignedDomainSequences[sequence_tag_b]

                except KeyError:
                    # For some reason we don't have the multiple alignment files.
                    # Try manual alignment
                    if shared_domain not in missing_aligned_domain_files and verbose:
                        # this will print everytime an unfound <domain>.algn is not found for every
                        # distance calculation (but at least, not for every domain pair!)
                        print("  Warning: " + shared_domain + ".algn not found. Trying pairwise alignment...")
                        missing_aligned_domain_files.append(shared_domain)

                    try:
                        unaligned_seqA = temp_domain_fastas[sequence_tag_a]
                        unaligned_seqB = temp_domain_fastas[sequence_tag_b]
                    except KeyError:
                        # parse the file for the first time and load all the sequences
                        with open(os.path.join(domains_folder, shared_domain + ".fasta"), "r") as domain_fasta_handle:
                            temp_domain_fastas = fasta_parser(domain_fasta_handle)

                        unaligned_seqA = temp_domain_fastas[sequence_tag_a]
                        unaligned_seqB = temp_domain_fastas[sequence_tag_b]

                    # gap_open = -15
                    # gap_extend = -6.67. These parameters were set up by Emzo
                    alignScore = pairwise2.align.globalds(unaligned_seqA, unaligned_seqB, scoring_matrix, -15, -6.67,
                                                          one_alignment_only=True)
                    bestAlignment = alignScore[0]
                    aligned_seqA = bestAlignment[0]
                    aligned_seqB = bestAlignment[1]

                # - Calculate aligned domain sequences similarity -
                # Sequences *should* be of the same length unless something went
                # wrong elsewhere
                if len(aligned_seqA) != len(aligned_seqB):
                    print(
                    "\tWARNING: mismatch in sequences' lengths while calculating sequence identity (" + shared_domain + ")")
                    print("\t  Specific domain 1: " + sequence_tag_a + " len: " + str(len(aligned_seqA)))
                    print("\t  Specific domain 2: " + sequence_tag_b + " len: " + str(len(aligned_seqB)))
                    seq_length = min(len(aligned_seqA), len(aligned_seqB))
                else:
                    seq_length = len(aligned_seqA)

                for position in range(seq_length):
                    if aligned_seqA[position] == aligned_seqB[position]:
                        if aligned_seqA[position] != "-":
                            matches += 1
                        else:
                            gaps += 1

                DistanceMatrix[domsa][domsb] = 1 - (float(matches) / float(seq_length - gaps))

        # Only use the best scoring pairs
        Hungarian = Munkres()
        BestIndexes = Hungarian.compute(DistanceMatrix)
        A_domain_indexes = [i for i, j in enumerate(A_domlist) if j == shared_domain]
        B_domain_indexes = [i for i, j in enumerate(B_domlist) if j == shared_domain]
        if max([len(A_domain_indexes), len(B_domain_indexes)]) > 1:
            for pair_id in range(len(BestIndexes)):
                A_domlist[A_domain_indexes[BestIndexes[pair_id][0]]] = A_domlist[A_domain_indexes[
                    BestIndexes[pair_id][0]]] + "-" + str(pair_id)
                B_domlist[B_domain_indexes[BestIndexes[pair_id][1]]] = B_domlist[B_domain_indexes[
                    BestIndexes[pair_id][1]]] + "-" + str(pair_id)

        accumulated_distance = sum([DistanceMatrix[bi[0]][bi[1]] for bi in BestIndexes])

        # the difference in number of domains accounts for the "lost" (or not duplicated) domains
        sum_seq_dist = (abs(num_copies_a - num_copies_b) + accumulated_distance)  # essentially 1-sim
        normalization_element = max(num_copies_a, num_copies_b)

        if shared_domain[:7] in anchor_domains:
            S_anchor += normalization_element
            domain_difference_anchor += sum_seq_dist
        else:
            S += normalization_element
            domain_difference += sum_seq_dist

    if S_anchor != 0 and S != 0:
        DSS_non_anchor = domain_difference / float(S)
        DSS_anchor = domain_difference_anchor / float(S_anchor)

        # Calculate proper, proportional weight to each kind of domain
        non_anchor_prct = S / float(S + S_anchor)
        anchor_prct = S_anchor / float(S + S_anchor)

        # boost anchor subcomponent and re-normalize
        non_anchor_weight = non_anchor_prct / (anchor_prct * anchorboost + non_anchor_prct)
        anchor_weight = anchor_prct * anchorboost / (anchor_prct * anchorboost + non_anchor_prct)

        # Use anchorboost parameter to boost percieved rDSS_anchor
        DSS = (non_anchor_weight * DSS_non_anchor) + (anchor_weight * DSS_anchor)

    elif S_anchor == 0:
        DSS_non_anchor = domain_difference / float(S)
        DSS_anchor = 0.0

        DSS = DSS_non_anchor

    else:  # only anchor domains were found
        DSS_non_anchor = 0.0
        DSS_anchor = domain_difference_anchor / float(S_anchor)

        DSS = DSS_anchor

    DSS = 1 - DSS  # transform into similarity

    # ADJACENCY INDEX
    # calculates the Tanimoto similarity of pairs of adjacent domains
    AI2 = 0.0;  AI2w = 0.0
    if len(A_domlist) < 2 or len(B_domlist) < 2:
        AI = 0.0
    elif len(A_domlist) < 4 or len(B_domlist) < 4:
        setA_pairs = set()
        for l in range(len(A_domlist) - 1):
            setA_pairs.add(tuple(sorted([A_domlist[l], A_domlist[l + 1]])))

        setB_pairs = set()
        for l in range(len(B_domlist) - 1):
            setB_pairs.add(tuple(sorted([B_domlist[l], B_domlist[l + 1]])))
        # same treatment as in Jaccard
        AI = float(len(setA_pairs.intersection(setB_pairs))) / float(len(setA_pairs.union(setB_pairs)))
    else:
        setA_pairs = set()
        for l in range(len(A_domlist) - 1):
            setA_pairs.add(tuple(sorted([A_domlist[l], A_domlist[l + 1]])))

        setB_pairs = set()
        for l in range(len(B_domlist) - 1):
            setB_pairs.add(tuple(sorted([B_domlist[l], B_domlist[l + 1]])))
        # same treatment as in Jaccard
        AI = float(len(setA_pairs.intersection(setB_pairs))) / float(len(setA_pairs.union(setB_pairs)))
        setA_pairs_2 = set()
        for l in range(len(A_domlist) - 2):
            setA_pairs_2.add(tuple(sorted([A_domlist[l], A_domlist[l + 2]])))
        setB_pairs_2 = set()
        for l in range(len(B_domlist) - 2):
            setB_pairs_2.add(tuple(sorted([B_domlist[l], B_domlist[l + 2]])))
        

        AI2 = float(len(setA_pairs_2.intersection(setB_pairs_2))) / float(len(setA_pairs_2.union(setB_pairs_2)))

        AI2w = AIw*0.25
        AIw = AIw*0.75
        
    try:
        Distance2 = 1 - (Jaccardw * Jaccard) - (DSSw * DSS) - (AIw * AI) - (AI2w * AI2)
        Distance = 1 - (Jaccardw * Jaccard) - (DSSw * DSS) - (AIw * AI)
        return Distance2, Jaccard, DSS, AI, DSS_non_anchor, DSS_anchor, S, S_anchor
    except SyntaxError:
        Distance = 1 - (Jaccardw * Jaccard) - (DSSw * DSS) - (AIw * AI)

    # This could happen due to numerical innacuracies
    if Distance < 0.0:
        if Distance < -0.000001:  # this definitely is something else...
            print("Negative distance detected!")
            print(Distance)
            print(A + " - " + B)
            print("J: " + str(Jaccard) + "\tDSS: " + str(DSS) + "\tAI: " + str(AI))
            print("Jw: " + str(Jaccardw) + "\tDSSw: " + str(DSSw) + "\tAIw: " + str(AIw))
        Distance = 0.0

    return Distance, Jaccard, DSS, AI, DSS_non_anchor, DSS_anchor, S, S_anchor

class FloatRange(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def __eq__(self, other):
        return self.start <= other <= self.end

    def __repr__(self):
        return '{0}-{1}'.format(self.start, self.end)

class bgc_data:
    def __init__(self, accession_id, description, product, records, max_width, biosynthetic_genes, contig_edge):
        # These two properties come from the genbank file:
        self.accession_id = accession_id
        self.description = description
        # AntiSMASH predicted class of compound:
        self.product = product
        # number of records in the genbank file (think of multi-locus BGCs):
        self.records = records
        # length of largest record (it will be used for ArrowerSVG):
        self.max_width = int(max_width)
        # Internal set of tags corresponding to genes that AntiSMASH marked
        # as "Kind: Biosynthetic". It is formed as
        # clusterName + "_ORF" + cds_number + ":gid:" + gene_id + ":pid:" + protein_id + ":loc:" + gene_start + ":" + gene_end + ":strand:" + {+,-}
        self.biosynthetic_genes = biosynthetic_genes
        # AntiSMASH 4+ marks BGCs that sit on the edge of a contig
        self.contig_edge = contig_edge

def CMD_parser():
    parser = ArgumentParser()

    parser.add_argument("-o", "--outputdir", dest="outputdir", default="", required=True,
                        help="Output directory, this will contain your pfd, pfs, network and hmmscan output files.")
    parser.add_argument("-db", "--dbdir", dest="dbdir", default="", required=True,
                        help="Path to the antismashdb database")
    parser.add_argument("-i", "--inputdir", dest="inputdir", default=os.path.dirname(os.path.realpath(__file__)),
                        help="Input directory of gbk files, if left empty, all gbk files in current and lower directories will be used.")
    parser.add_argument("-c", "--cores", dest="cores", default=cpu_count(),
                        help="Set the number of cores the script may use (default: use all available cores)")
    parser.add_argument("-v", "--verbose", dest="verbose", action="store_true", default=False,
                        help="Prints more detailed information. Toggle to true.")
    parser.add_argument("--include_singletons", dest="include_singletons", action="store_true", default=False,
                        help="Include nodes that have no edges to other nodes from the network. Toggle to activate.")
    parser.add_argument("-d", "--domain_overlap_cutoff", dest="domain_overlap_cutoff", default=0.1,
                        help="Specify at which overlap percentage domains are considered to overlap.")
    parser.add_argument("-m", "--min_bgc_size", dest="min_bgc_size", default=0,
                        help="Provide the minimum size of a BGC to be included in the analysis. Default is 0 base pairs")

    parser.add_argument("-s", "--samples", dest="samples", action="store_true", default=False,
                        help="Separate the input files into samples according to their containing folder within the input folder. Toggle to activate")

    parser.add_argument("--no_all", dest="no_all", action="store_true", default=False,
                        help="By default, BiG-SCAPE uses a single data set comprised of all input files available recursively within the input folder. Toggle to disactivate this behaviour (in that case, if the --samples parameter is not activated, BiG-SCAPE will not create any network file)")

    parser.add_argument("--mix", dest="mix", action="store_true", default=False,
                        help="By default, BiG-SCAPE separates the analysis according to the BGC product (PKS Type I, NRPS, RiPPs, etc.) and will create network directories for each class. Toggle to include an analysis mixing all classes")

    parser.add_argument("--cluster_family", dest="cluster_family", action="store_true", default=False,
                        help="BiG-SCAPE will perform a second layer of clustering and attempt to group families assigned from clustering with cutoff of 0.5 to clans")

    parser.add_argument("--clan_cutoff", dest="clan_cutoff", default=0.5,
                        help="Distance Cutoff to use for Family Clustering")

    parser.add_argument("--hybrids", dest="hybrids", action="store_true",
                        default=False, help="Toggle to also add BGCs with hybrid\
                        predicted products from the PKS/NRPS Hybrids and Others\
                        classes to each subclass (e.g. a 'terpene-nrps' BGC from\
                        Others would be added to the Terpene and NRPS classes")

    parser.add_argument("--local", dest="local", action="store_true", default=False,
                        help="Activate local mode. BiG-SCAPE will change the logic in the distance calculation phase to try to perform local alignments of shorter, 'fragmented' BGCs by finding the maximum overlap in domain content.")

    parser.add_argument("--no_classify", dest="no_classify", action="store_true", default=False,
                        help="By default, BiG-SCAPE classifies the output files analysis based on the BGC product. Toggle to deactivate (note that if the --mix parameter is not activated, BiG-SCAPE will not create any network file).")

    parser.add_argument("--banned_classes", nargs='+', dest="banned_classes", default=[],
                        choices=["PKSI", "PKSother", "NRPS", "RiPPs", "Saccharides", "Terpene", "PKS-NRP_Hybrids",
                                 "Others"],
                        help="Classes that should NOT be included in the classification. E.g. \"--banned_classes PKSI PKSOther\"")

    parser.add_argument("--pfam_dir", dest="pfam_dir",
                        default=os.path.dirname(os.path.realpath(__file__)),
                        help="Location of hmmpress-processed Pfam files. Default is same location of BiG-SCAPE")
    parser.add_argument("--anchorfile", dest="anchorfile", default="anchor_domains.txt",
                        help="Provide a custom location for the anchor domains file, default is anchor_domains.txt.")
    parser.add_argument("--exclude_gbk_str", dest="exclude_gbk_str", default="",
                        help="If this string occurs in the gbk filename, this file will not be used for the analysis.")

    parser.add_argument("--mafft_pars", dest="mafft_pars", default="",
                        help="Add single/multiple parameters for MAFFT specific enclosed by quotation marks e.g. \"--nofft --parttree\"")
    parser.add_argument("--al_method", dest="al_method", default="--retree 2",
                        help="alignment method for MAFFT, if there's a space in the method's name, enclose by quotation marks. default: \"--retree 2\" corresponds to the FFT-NS-2 method")
    parser.add_argument("--maxiterate", dest="maxit", default=1000,
                        help="Maxiterate parameter in MAFFT, default is 1000, corresponds to the FFT-NS-2 method")
    parser.add_argument("--mafft_threads", dest="mafft_threads", default=0,
                        help="Set the number of threads in MAFFT, -1 sets the number of threads as the number of physical cores. Default: same as --cores parameter")
    parser.add_argument("--use_mafft", dest="use_mafft", action="store_true", default=False,
                        help="Use MAFFT instead of hmmalign for multiple alignment of domain sequences")
    parser.add_argument("--force_hmmscan", dest="force_hmmscan", action="store_true", default=False,
                        help="Force domain prediction using hmmscan even if BiG-SCAPE finds processed domtable files (e.g. to use a new version of PFAM).")
    parser.add_argument("--skip_ma", dest="skip_ma", action="store_true", default=False,
                        help="Skip multiple alignment of domains' sequences. Use if alignments have been generated in a previous run.")
    parser.add_argument("--skip_all", dest="skip_all", action="store_true",
                        default=False, help="Only generate new network files. ")
    parser.add_argument("--cutoffs", dest="cutoffs", nargs="+",
                        default=[0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50, 0.55, 0.60, 0.65, 0.70, 0.75,
                                 0.80], type=float, choices=[FloatRange(0.0, 1.0)],
                        help="Generate networks using multiple raw distance cutoff values, example: \"0.1, 0.25, 0.5, 1.0\". Default: all values from 0.10 to 0.80 with 0.05 intervals.")

    options = parser.parse_args()
    return options


def write_network_matrix(network_matrix, cutoffs_and_filenames, include_singletons, clusterNames, bgc_info):
    """
    An entry in the distance matrix is currently (all floats):
      0         1           2      3      4       5    6    7    8      9     10   11
    clus1Idx clus2Idx bgcClassIdx rawD  sqrtSim  Jac  DSS  AI rDSSna  rDSSa   S    Sa

    The final row in the network file is currently:
      0      1      2     3      4   5   6     7       8    9   10    11       12
    clus1  clus2  rawD  sqrtSim  J  DSS  AI  rDSSna  rDSSa  S   Sa  combGrp  ShrdGrp
    """

    # Open file handles for each cutoff
    networkfiles = {}
    cutoffs, filenames = zip(*cutoffs_and_filenames)
    for cutoff, filename in cutoffs_and_filenames:
        networkfiles[cutoff] = open(filename, "w")
        networkfiles[cutoff].write(
            "Clustername 1\tClustername 2\tRaw distance\tSquared similarity\tJaccard index\tDSS index\tAdjacency index\traw DSS non-anchor\traw DSS anchor\tNon-anchor domains\tAnchor domains\tCombined group\tShared group\n")

    # Dictionaries to keep track of connected nodes, to know which are singletons
    clusterSetAllDict = {}
    clusterSetConnectedDict = {}
    for cutoff in cutoffs:
        clusterSetAllDict[cutoff] = set()
        clusterSetConnectedDict[cutoff] = set()

    for matrix_entry in network_matrix:
        gc1 = clusterNames[int(matrix_entry[0])]
        gc2 = clusterNames[int(matrix_entry[1])]
        row = [gc1, gc2]

        # get AntiSMASH annotations
        clus1group = bgc_info[gc1].product
        clus2group = bgc_info[gc2].product

        # add all the other floats
        row.extend(matrix_entry[3:-2])

        # add number of anchor/non-anchor domains as integers
        row.append(int(matrix_entry[-2]))
        row.append(int(matrix_entry[-1]))

        # prepare combined group
        if clus1group != "" and clus2group != "":  # group1, group2
            row.append(" - ".join(sorted([clus1group, clus2group])))
        elif clus2group != "":
            row.append(clus2group)
        elif clus1group != "":
            row.append(clus1group)
        else:
            row.append("NA")

        # prepare share group (if they indeed share it)
        if clus1group == clus2group:
            row.append(clus1group)
        else:
            row.append("")

        for cutoff in cutoffs:
            clusterSetAllDict[cutoff].add(gc1)
            clusterSetAllDict[cutoff].add(gc2)

            if row[2] < cutoff:
                clusterSetConnectedDict[cutoff].add(gc1)
                clusterSetConnectedDict[cutoff].add(gc2)

                networkfiles[cutoff].write("\t".join(map(str, row)) + "\n")

    # Add the nodes without any edges, give them an edge to themselves with a distance of 0
    if include_singletons == True:
        for cutoff in cutoffs:
            for gc in clusterSetAllDict[cutoff] - clusterSetConnectedDict[cutoff]:
                # Arbitrary numbers for S and Sa domains: 1 of each (logical would be 0,0 but
                # that could mess re-analysis with divisions-by-zero;
                networkfiles[cutoff].write(
                    "\t".join([gc, gc, "0", "1", "1", "1", "1", "0", "0", "1", "1", "", ""]) + "\n")

    # Close all files
    for networkfile in networkfiles.values():
        networkfile.close()


if __name__ == '__main__':
    options = CMD_parser()
    global cores
    global local
    global clusterNames, bgcClassNames
    options_all = not options.no_all
    options_classify = not options.no_classify
    output_folder = str(options.outputdir)
    db_folder = str(options.dbdir) + '/processed_antismashdb/'
    cutoff_list = options.cutoffs
    networks_folder_all = "networks_all"
    networks_folder_samples = "networks_samples"
    bgc_class_weight = {}
    bgc_class_weight["PKSI"] = (0.22, 0.76, 0.02, 1.0)
    bgc_class_weight["PKSother"] = (0.0, 0.32, 0.68, 4.0)
    bgc_class_weight["NRPS"] = (0.0, 0.75, 0.25, 4.0)
    bgc_class_weight["RiPPs"] = (0.28, 0.71, 0.01, 1.0)
    bgc_class_weight["Saccharides"] = (0.0, 0.0, 1.0, 1.0)
    bgc_class_weight["Terpene"] = (0.2, 0.75, 0.05, 2.0)
    bgc_class_weight["PKS-NRP_Hybrids"] = (0.0, 0.78, 0.22, 1.0)
    bgc_class_weight["Others"] = (0.01, 0.97, 0.02, 4.0)
    bgc_class_weight["Custom"] = (0.0, 1.0, 0.0, 4.0)
    bgcClassNames = tuple(sorted(list(bgc_class_weight)) + ["mix"])
    assert bgcClassNames[-1] == 'mix'

    bgcClassName2idx = dict(zip(bgcClassNames, range(len(bgcClassNames))))
    cores = int(options.cores)
    # define which classes will be analyzed (if in the options_classify mode)
    valid_classes = set()
    for key in bgc_class_weight:
        valid_classes.add(key.lower())
    user_banned_classes = set([a.strip().lower() for a in options.banned_classes])
    valid_classes = valid_classes - user_banned_classes

    for c in cutoff_list:
        if c <= 0.0 or c > 1.0:
            cutoff_list.remove(c)

    domains_folder = os.path.join(output_folder, "domains")

    query_bgc_info = pickle.load(open(output_folder + 'bgc_info.p'))
    query_DomainList = pickle.load(open(output_folder + 'DomainList.p'))
    query_AlignedDomainSequences = pickle.load(open(output_folder + 'AlignedDomainSequences.p'))
    query_genbankDict = pickle.load(open(output_folder + 'genbankdict.p'))
    query_BGCs = pickle.load(open(output_folder + "BGCs.dict"))

    # set valid_classes to all the classes contained in the query [usually nrps and pks]
    clusters = query_genbankDict.keys()
    clusterNames = tuple(sorted(list(clusters)))
    clusterNames2idx = dict(zip(clusterNames, range(len(clusterNames))))
    valid_classes = []
    for clusterIdx, cluster in enumerate(clusterNames):
        product = query_bgc_info[cluster].product
        predicted_class = sort_bgc(product)
        valid_classes.append(predicted_class.lower())
    valid_classes = set(valid_classes)
    db_bgc_info = pickle.load(open(db_folder + 'bgc_info.p'))
    db_DomainList = pickle.load(open(db_folder + 'DomainList.p'))
    db_AlignedDomainSequences = pickle.load(open(db_folder + 'AlignedDomainSequences.p'))
    db_genbankDict = pickle.load(open(db_folder + 'genbankdict.p'))
    db_BGCs = pickle.load(open(db_folder + "BGCs.dict"))


    genbankDict = merge_two_dicts(query_genbankDict, db_genbankDict)
    BGCs = merge_two_dicts(query_BGCs, db_BGCs)
    AlignedDomainSequences = merge_two_dicts(query_AlignedDomainSequences, db_AlignedDomainSequences)
    DomainList = merge_two_dicts(query_DomainList, db_DomainList)
    bgc_info= merge_two_dicts(query_bgc_info, db_bgc_info)

    clusters = genbankDict.keys()
    clusterNames = tuple(sorted(list(clusters)))
    clusterNames2idx = dict(zip(clusterNames, range(len(clusterNames))))

    global anchor_domains
    if os.path.isfile(options.anchorfile):
        anchor_domains = get_anchor_domains(options.anchorfile)
    else:
        print("File with list of anchor domains not found")
        anchor_domains = set()

    include_singletons = options.include_singletons

    # Try to make default analysis using all files found inside the input folder
    if options_all:
        print("\nGenerating distance network files with ALL available input files")

        # create output directory
        create_directory(os.path.join(output_folder, networks_folder_all), "Networks_all", False)

        # Making network files separating by BGC class
        if options_classify:
            print("\n Working for each BGC class")

            # reinitialize BGC_classes to make sure the bgc lists are empty
            BGC_classes = defaultdict(list)

            # Preparing gene cluster classes
            print("  Sorting the input BGCs\n")

            for clusterIdx, cluster in enumerate(clusterNames):
                product = bgc_info[cluster].product
                predicted_class = sort_bgc(product)
                if predicted_class.lower() in valid_classes:
                    BGC_classes[predicted_class].append(clusterIdx)
                # possibly add hybrids to 'pure' classes
                if options.hybrids:
                    if predicted_class == "PKS-NRP_Hybrids":
                        if "nrps" in valid_classes:
                            BGC_classes["NRPS"].append(clusterIdx)
                        if "t1pks" in product and "pksi" in valid_classes:
                            BGC_classes["PKSI"].append(clusterIdx)
                        if "t1pks" not in product and "pksother" in valid_classes:
                            BGC_classes["PKSother"].append(clusterIdx)

                    if predicted_class == "Others" and "-" in product:
                        subclasses = set()
                        for subproduct in product.split("-"):
                            subclass = sort_bgc(subproduct)
                            if subclass.lower() in valid_classes:
                                subclasses.add(subclass)

                        for subclass in subclasses:
                            BGC_classes[subclass].append(clusterIdx)
                        subclasses.clear()
            # only make folders for the BGC_classes that are found
            for bgc_class in BGC_classes:
                folder_name = bgc_class

                print("\n  " + folder_name + " (" + str(len(BGC_classes[bgc_class])) + " BGCs)")

                # create output directory
                create_directory(os.path.join(output_folder, networks_folder_all, folder_name), "  All - " + bgc_class,
                                 False)

                # Create an additional file with the final list of all clusters in the class
                print("   Writing annotation files")
                path_list = os.path.join(output_folder, networks_folder_all, folder_name,
                                         "Network_Annotations_All_" + folder_name + ".tsv")
                with open(path_list, "w") as list_file:
                    list_file.write("BGC\tAccesion ID\tDescription\tProduct Prediction\tBiG-SCAPE class\n")
                    for idx in BGC_classes[bgc_class]:
                        bgc = clusterNames[idx]
                        product = bgc_info[bgc].product
                        list_file.write("\t".join([bgc, bgc_info[bgc].accession_id, bgc_info[bgc].description, product,
                                                   sort_bgc(product)]) + "\n")
                print (BGC_classes)
                if len(BGC_classes[bgc_class]) > 1:
                    print("   Calculating all pairwise distances")
                    k = query_genbankDict.keys()
                    k = [x for x in k if 'final' not in x]
                    pairs = set(map(tuple, map(sorted, combinations(BGC_classes[bgc_class], 2))))
                    pairs = set([x for x in pairs if (clusterNames[x[0]] in k and clusterNames[x[1]] not in k) or (clusterNames[x[1]] in k and clusterNames[x[0]] not in k)])
                    if len(pairs) > 0:
                        print (bgc_class)
                        del BGC_classes[bgc_class][:]
                        cluster_pairs = [(x, y, bgcClassName2idx[bgc_class]) for (x, y) in pairs]
                        pairs.clear()
                        network_matrix = generate_network(cluster_pairs, cores)
                        del cluster_pairs[:]

                        print("   Writing output files")
                        pathBase = os.path.join(output_folder, networks_folder_all, folder_name, "all" + folder_name)
                        print (pathBase)
                        filenames = []
                        for cutoff in cutoff_list:
                            filenames.append(pathBase + "_c%.2f.network" % cutoff)
                        cutoffs_and_filenames = zip(cutoff_list, filenames)
                        del filenames[:]

                        write_network_matrix(network_matrix, cutoffs_and_filenames, include_singletons, clusterNames,
                                             bgc_info)

                        del network_matrix[:]
