#!/usr/bin/env python

"""
PI: Marnix Medema               marnix.medema@wur.nl

Developers:
Jorge Navarro                   jorge.navarromunoz@wur.nl
Emmanuel (Emzo) de los Santos   E.De-Los-Santos@warwick.ac.uk
Marley Yeong                    marleyyeong@live.nl
Vittorio Tracanna               vittorio.tracanna@wur.nl


Dependencies: hmmer, biopython, (mafft), munkres.py

See more info on
https://git.wageningenur.nl/medema-group/biosyntheticSpadesRankingPipeline

# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.
"""

import cPickle as pickle
import os
import subprocess
import sys
import time
from glob import glob
from collections import defaultdict
from multiprocessing import Pool, cpu_count
from argparse import ArgumentParser

from Bio import SeqIO
from functions import *
from ArrowerSVG import *


def CMD_parser():
    parser = ArgumentParser()

    parser.add_argument("-o", "--outputdir", dest="outputdir", default="", required=True,
                        help="Output directory, this will contain your pfd, pfs, network and hmmscan output files.")
    parser.add_argument("-i", "--inputdir", dest="inputdir", default=os.path.dirname(os.path.realpath(__file__)),
                        help="Input directory of gbk files, if left empty, all gbk files in current and lower directories will be used.")
    parser.add_argument("-c", "--cores", dest="cores", default=cpu_count(),
                        help="Set the number of cores the script may use (default: use all available cores)")
    parser.add_argument("-v", "--verbose", dest="verbose", action="store_true", default=False,
                        help="Prints more detailed information. Toggle to true.")
    parser.add_argument("--include_singletons", dest="include_singletons", action="store_true", default=False,
                        help="Include nodes that have no edges to other nodes from the network. Toggle to activate.")
    parser.add_argument("-d", "--domain_overlap_cutoff", dest="domain_overlap_cutoff", default=0.1,
                        help="Specify at which overlap percentage domains are considered to overlap.")
    parser.add_argument("-m", "--min_bgc_size", dest="min_bgc_size", default=0,
                        help="Provide the minimum size of a BGC to be included in the analysis. Default is 0 base pairs")

    parser.add_argument("-s", "--samples", dest="samples", action="store_true", default=False,
                        help="Separate the input files into samples according to their containing folder within the input folder. Toggle to activate")

    parser.add_argument("--no_all", dest="no_all", action="store_true", default=False,
                        help="By default, BiG-SCAPE uses a single data set comprised of all input files available recursively within the input folder. Toggle to disactivate this behaviour (in that case, if the --samples parameter is not activated, BiG-SCAPE will not create any network file)")

    parser.add_argument("--mix", dest="mix", action="store_true", default=False,
                        help="By default, BiG-SCAPE separates the analysis according to the BGC product (PKS Type I, NRPS, RiPPs, etc.) and will create network directories for each class. Toggle to include an analysis mixing all classes")

    parser.add_argument("--cluster_family", dest="cluster_family", action="store_true", default=False,
                        help="BiG-SCAPE will perform a second layer of clustering and attempt to group families assigned from clustering with cutoff of 0.5 to clans")

    parser.add_argument("--clan_cutoff", dest="clan_cutoff", default=0.5,
                        help="Distance Cutoff to use for Family Clustering")

    parser.add_argument("--hybrids", dest="hybrids", action="store_true",
                        default=False, help="Toggle to also add BGCs with hybrid\
                        predicted products from the PKS/NRPS Hybrids and Others\
                        classes to each subclass (e.g. a 'terpene-nrps' BGC from\
                        Others would be added to the Terpene and NRPS classes")

    parser.add_argument("--local", dest="local", action="store_true", default=False,
                        help="Activate local mode. BiG-SCAPE will change the logic in the distance calculation phase to try to perform local alignments of shorter, 'fragmented' BGCs by finding the maximum overlap in domain content.")

    parser.add_argument("--no_classify", dest="no_classify", action="store_true", default=False,
                        help="By default, BiG-SCAPE classifies the output files analysis based on the BGC product. Toggle to deactivate (note that if the --mix parameter is not activated, BiG-SCAPE will not create any network file).")

    parser.add_argument("--banned_classes", nargs='+', dest="banned_classes", default=[],
                        choices=["PKSI", "PKSother", "NRPS", "RiPPs", "Saccharides", "Terpene", "PKS-NRP_Hybrids",
                                 "Others"],
                        help="Classes that should NOT be included in the classification. E.g. \"--banned_classes PKSI PKSOther\"")

    parser.add_argument("--pfam_dir", dest="pfam_dir",
                        default=os.path.dirname('/mnt/scratch/traca001/tools/BiG-SCAPE/'),
                        help="Location of hmmpress-processed Pfam files. Default is same location of BiG-SCAPE")
    parser.add_argument("--anchorfile", dest="anchorfile", default="anchor_domains.txt",
                        help="Provide a custom location for the anchor domains file, default is anchor_domains.txt.")
    parser.add_argument("--exclude_gbk_str", dest="exclude_gbk_str", default="",
                        help="If this string occurs in the gbk filename, this file will not be used for the analysis.")

    parser.add_argument("--mafft_pars", dest="mafft_pars", default="",
                        help="Add single/multiple parameters for MAFFT specific enclosed by quotation marks e.g. \"--nofft --parttree\"")
    parser.add_argument("--al_method", dest="al_method", default="--retree 2",
                        help="alignment method for MAFFT, if there's a space in the method's name, enclose by quotation marks. default: \"--retree 2\" corresponds to the FFT-NS-2 method")
    parser.add_argument("--maxiterate", dest="maxit", default=1000,
                        help="Maxiterate parameter in MAFFT, default is 1000, corresponds to the FFT-NS-2 method")
    parser.add_argument("--mafft_threads", dest="mafft_threads", default=0,
                        help="Set the number of threads in MAFFT, -1 sets the number of threads as the number of physical cores. Default: same as --cores parameter")
    parser.add_argument("--use_mafft", dest="use_mafft", action="store_true", default=False,
                        help="Use MAFFT instead of hmmalign for multiple alignment of domain sequences")
    parser.add_argument("--force_hmmscan", dest="force_hmmscan", action="store_true", default=False,
                        help="Force domain prediction using hmmscan even if BiG-SCAPE finds processed domtable files (e.g. to use a new version of PFAM).")
    parser.add_argument("--skip_ma", dest="skip_ma", action="store_true", default=False,
                        help="Skip multiple alignment of domains' sequences. Use if alignments have been generated in a previous run.")
    parser.add_argument("--skip_all", dest="skip_all", action="store_true",
                        default=False, help="Only generate new network files. ")
    parser.add_argument("--cutoffs", dest="cutoffs", nargs="+",
                        default=[0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50, 0.55, 0.60, 0.65, 0.70, 0.75,
                                 0.80], type=float, choices=[FloatRange(0.0, 1.0)],
                        help="Generate networks using multiple raw distance cutoff values, example: \"0.1, 0.25, 0.5, 1.0\". Default: all values from 0.10 to 0.80 with 0.05 intervals.")

    options = parser.parse_args()
    return options


def filter_gbk_files(inputdir):
    """Checks all gbk files to see if they belong to nrps, pks or transpks
    :return: list of files to copy in new folder
    """
    products_filter = set(['nrps', 't1pks', 'transatpks'])
    current_dir = ""
    newdirpath = '/'.join(inputdir.split('/')[:-1]) + '_filtered/' + inputdir.split('/')[-1]
    if not os.path.exists(newdirpath):
        os.mkdir(newdirpath)
    for dirpath, dirnames, filenames in os.walk(inputdir):
        head, tail = os.path.split(dirpath)
        if current_dir != tail:
            current_dir = tail
        for fname in filenames:
            clusterName = fname[:-4]
            if not fname.endswith('.final'):
                records = list(SeqIO.parse(os.path.join(dirpath, fname), "genbank"))
            else:
                records = list()
            for record in records:
                for feature in record.features:
                    if feature.type == "cluster":
                        if len(set(feature.qualifiers["product"][0].split('-')).intersection(products_filter)) > 0:
                            copy_file(dirpath, dirnames, fname, newdirpath)
    return newdirpath


def copy_file(dirpath, dirnames, fname, newdirpath):
    if not os.path.isfile(newdirpath + fname):
        cp_cmd = "cp " + dirpath + '/' + fname + " " + newdirpath + fname
        os.system(cp_cmd)


def get_gbk_files(inputdir, bgc_fasta_folder, min_bgc_size, exclude_gbk_str, bgc_info):
    """Searches given directory for genbank files recursively, will assume that
    the genbank files that have the same name are the same genbank file. 
    Returns a dictionary that contains the names of the clusters found as keys
    and a list that contains [0] a path to the genbank file and [1] the 
    samples that the genbank file is a part of.
    Extract and write the sequences as fasta files if not already in the Fasta 
    folder.
    return: {cluster_name:[genbank_path,[s_a,s_b...]]}
    """
    genbankDict = {}

    file_counter = 0
    processed_sequences = 0
    biosynthetic_genes = set()
    product_list_per_record = []
    fasta_data = []
    save_fasta = True
    contig_edge = False
    products_filter = set(['nrps', 't1pks', 'transatpks'])
    print("\nImporting GenBank files")
    if exclude_gbk_str != "":
        print(" Skipping files with '" + exclude_gbk_str + "' in their filename")

    current_dir = ""
    for dirpath, dirnames, filenames in os.walk(inputdir):
        head, tail = os.path.split(dirpath)

        if current_dir != tail:
            current_dir = tail

        genbankfilelist = []

        filenames = [x for x in filenames if not x.endswith('.final.gbk')]

        for fname in filenames:
            if fname[-3:] != "gbk":
                continue

            clusterName = fname[:-4]

            if exclude_gbk_str != "" and exclude_gbk_str in fname:
                print(" Skipping file " + fname)
                continue
            if "_ORF" in fname:
                print(" Skipping file " + fname + " (string '_ORF' is used internally)")
                continue

            if " " in fname:
                sys.exit(
                    "\nError: Input GenBank files should not have spaces in their filenames as HMMscan cannot process them properly ('too many arguments').")

            # See if we need to write down the sequence
            outputfile = os.path.join(bgc_fasta_folder, clusterName + '.fasta')
            if os.path.isfile(outputfile) and os.path.getsize(outputfile) > 0:
                save_fasta = False
            else:
                save_fasta = True

            try:
                # basic file verification. Substitutes check_data_integrity
                records = list(SeqIO.parse(os.path.join(dirpath, fname), "genbank"))
            except ValueError as e:
                print("   Error with file " + os.path.join(dirpath, fname) + ": \n    '" + str(e) + "'")
                print("    (This file will be excluded from the analysis)")
                continue
            else:
                bgc_size = 0
                cds_ctr = 0
                product = "no type"
                del product_list_per_record[:]

                max_width = 0  # This will be used for the SVG figure
                record_count = 0

                for record in records:
                    skip = False
                    record_count += 1
                    bgc_size += len(record.seq)
                    if len(record.seq) > max_width:
                        max_width = len(record.seq)

                    for feature in record.features:
                        if "cluster" in feature.type and "product" in feature.qualifiers:
                            if len(feature.qualifiers["product"]) > 1:
                                print(
                                    "  WARNING: more than product annotated in record " + str(
                                        record_count) + ", " + fname)
                                break
                            else:
                                product_list_per_record.append(feature.qualifiers["product"][0].replace(" ", ""))

                        # Get biosynthetic genes + sequences
                        if feature.type == "CDS":
                            cds_ctr += 1

                            CDS = feature
                            gene_id = ""
                            if "gene" in CDS.qualifiers:
                                # In principle, we should keep a list of genes with
                                # the same id (isoforms) and only keep the largest
                                # TODO
                                gene_id = CDS.qualifiers.get('gene', "")[0]

                            protein_id = ""
                            if "protein_id" in CDS.qualifiers:
                                protein_id = CDS.qualifiers.get('protein_id', "")[0]

                            # nofuzzy_start/nofuzzy_end are obsolete
                            # http://biopython.org/DIST/docs/api/Bio.SeqFeature.FeatureLocation-class.html#nofuzzy_start
                            gene_start = max(0, int(CDS.location.start))
                            gene_end = max(0, int(CDS.location.end))
                            direction = CDS.location.strand

                            if direction == 1:
                                strand = '+'
                            else:
                                strand = '-'

                            fasta_header = clusterName + "_ORF" + str(cds_ctr) + ":gid:" + str(gene_id) + ":pid:" + str(
                                protein_id) + ":loc:" + str(gene_start) + ":" + str(gene_end) + ":strand:" + strand
                            fasta_header = fasta_header.replace(">",
                                                                "")  # the coordinates might contain larger than signs, tools upstream don't like this
                            fasta_header = fasta_header.replace(" ",
                                                                "")  # the domtable output format (hmmscan) uses spaces as a delimiter, so these cannot be present in the fasta header

                            if "sec_met" in feature.qualifiers:
                                if "Kind: biosynthetic" in feature.qualifiers["sec_met"]:
                                    biosynthetic_genes.add(fasta_header)

                            fasta_header = ">" + fasta_header

                            if save_fasta:
                                if 'translation' in CDS.qualifiers.keys():
                                    prot_seq = CDS.qualifiers['translation'][0]
                                # If translation isn't available translate manually, this will take longer
                                else:
                                    nt_seq = CDS.location.extract(record.seq)

                                    # If we know sequence is an ORF (like all CDSs), codon table can be
                                    #  used to correctly translate alternative start codons.
                                    #  see http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc25
                                    # If the sequence has a fuzzy start/end, it might not be complete,
                                    # (therefore it might not be the true start codon)
                                    # However, in this case, if 'translation' not available, assume
                                    #  this is just a random sequence
                                    complete_cds = False

                                    # More about fuzzy positions
                                    # http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc39
                                    fuzzy_start = False
                                    if str(CDS.location.start)[0] in "<>":
                                        complete_cds = False
                                        fuzzy_start = True

                                    fuzzy_end = False
                                    if str(CDS.location.end)[0] in "<>":
                                        fuzzy_end = True

                                    # for protein sequence if it is at the start of the entry assume
                                    # that end of sequence is in frame and trim from the beginning
                                    # if it is at the end of the genbank entry assume that the start
                                    # of the sequence is in frame
                                    reminder = len(nt_seq) % 3
                                    if reminder > 0:
                                        if fuzzy_start and fuzzy_end:
                                            print(
                                                "Warning, CDS (" + clusterName + ", " +
                                                CDS.qualifiers.get('locus_tag', "")[
                                                    0] + ") has fuzzy start and end positions, and a sequence length not multiple of three. Skipping")
                                            break

                                        if fuzzy_start:
                                            if reminder == 1:
                                                nt_seq = nt_seq[1:]
                                            else:
                                                nt_seq = nt_seq[2:]
                                        # fuzzy end
                                        else:
                                            # same logic reverse direction
                                            if reminder == 1:
                                                nt_seq = nt_seq[:-1]
                                            else:
                                                nt_seq = nt_seq[:-2]

                                    # The Genetic Codes: www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi
                                    if "transl_table" in CDS.qualifiers.keys():
                                        CDStable = CDS.qualifiers.get("transl_table", "")[0]
                                        prot_seq = str(nt_seq.translate(table=CDStable, to_stop=True, cds=complete_cds))
                                    else:
                                        prot_seq = str(nt_seq.translate(to_stop=True, cds=complete_cds))

                                fasta_data.append((fasta_header, prot_seq))

                if bgc_size > min_bgc_size:  # exclude the bgc if it's too small
                    file_counter += 1
                    # check what we have product-wise
                    # In particular, handle different products for multi-record files
                    product_set = set(product_list_per_record)
                    if len(product_set) == 1:  # only one type of product
                        product = product_list_per_record[0]
                    elif "other" in product_set:  # more than one, and it contains "other"
                        if len(product_set) == 2:
                            product = list(product_set - set(['other']))[0]  # product = not "other"
                        else:
                            product = "-".join(product_set - set(['other']))  # likely a hybrid
                    else:
                        product = "-".join(product_set)  # likely a hybrid

                    # assuming that the definition field is the same in all records
                    # product: antiSMASH predicted class of metabolite
                    # gbk definition
                    # number of records (for Arrower figures)
                    # max_width: width of the largest record (for Arrower figures)
                    # id: the GenBank's accession
                    # bgc_info[clusterName] = (product, records[0].description, len(records), max_width, records[0].id, biosynthetic_genes.copy())
                    # TODO contig_edge annotation is not present for antiSMASH v < 4
                    # Perhaps we can try to infer if it's in a contig edge: if
                    # - first biosynthetic gene start < 10kb or
                    # - max_width - last biosynthetic gene end < 10kb (but this will work only for the largest record)
                    bgc_info[clusterName] = bgc_data(records[0].id, records[0].description, product, len(records),
                                                     max_width, biosynthetic_genes.copy(), contig_edge)

                    if len(bgc_info[clusterName].biosynthetic_genes) == 0:
                        print("  Warning: No Biosynthetic Genes found in " + clusterName)

                    # TODO why re-process everything if it was already in the list?
                    # if name already in genbankDict.keys -> add current_dir
                    # else: extract all info
                    if clusterName in genbankDict.keys():
                        # Name was already in use. Use current_dir as the new sample's name
                        genbankDict[clusterName][1].add(current_dir)
                    else:
                        # location of first instance of the file is genbankDict[clustername][0]
                        genbankDict.setdefault(clusterName, [os.path.join(dirpath, fname), set([current_dir])])

                        # See if we need to write down the sequence
                        outputfile = os.path.join(bgc_fasta_folder, clusterName + '.fasta')
                        if os.path.isfile(outputfile) and os.path.getsize(outputfile) > 0:
                            processed_sequences += 1
                        else:
                            with open(outputfile, 'w') as fastaHandle:
                                for header_sequence in fasta_data:
                                    fastaHandle.write('%s\n' % header_sequence[0])
                                    fastaHandle.write('%s\n' % header_sequence[1])

                    if verbose:
                        print("  Adding " + fname + " (" + str(bgc_size) + " bps)")

                else:
                    print(" Discarding " + clusterName + " (size less than " + str(min_bgc_size) + " bp, was " + str(
                        bgc_size) + ")")

                del fasta_data[:]
                biosynthetic_genes.clear()

    if file_counter == 0:
        sys.exit("\nError: There are no files to process")

    #if file_counter == 1:
    #    sys.exit("\nError: Only one file found. Please input at least two files")

    print("\n Starting with " + str(file_counter) + " files")
    print(" Files that had its sequence extracted: " + str(file_counter - processed_sequences))
    if not os.path.isfile(output_folder + 'bgc_info.p'):
        pickle.dump(bgc_info, open(output_folder + 'bgc_info.p', "w"))
    return genbankDict


def runHmmScan(fastaPath, hmmPath, outputdir, verbose):
    """ Runs hmmscan command on a fasta file with a single core to generate a
    domtable file"""
    hmmFile = os.path.join(hmmPath, "Pfam-A.hmm")
    if os.path.isfile(fastaPath):
        name = ".".join(fastaPath.split(os.sep)[-1].split(".")[:-1])
        outputName = os.path.join(outputdir, name + ".domtable")

        hmmscan_cmd = "hmmscan --cpu 0 --domtblout %s --cut_tc %s %s" % (outputName, hmmFile, fastaPath)
        if verbose == True:
            print("   " + hmmscan_cmd)
        subprocess.check_output(hmmscan_cmd, shell=True)

    else:
        sys.exit("Error running hmmscan: Fasta file " + fastaPath + " doesn't exist")


def parseHmmScan(hmmscanResults, pfd_folder, pfs_folder, overlapCutoff):
    outputbase = ".".join(hmmscanResults.split(os.sep)[-1].split(".")[:-1])
    # try to read the domtable file to find out if this gbk has domains. Domains
    # need to be parsed into fastas anyway.
    if os.path.isfile(hmmscanResults):
        pfd_matrix = domtable_parser(outputbase, hmmscanResults)

        # get number of domains to decide if this BGC should be removed
        num_domains = len(pfd_matrix)

        if num_domains > 0:
            print("  Processing domtable file: " + outputbase)

            # check_overlap also sorts the filtered_matrix results and removes
            # overlapping domains, keeping the highest scoring one
            filtered_matrix, domains = check_overlap(pfd_matrix, overlapCutoff)

            # Save list of domains per BGC
            pfsoutput = os.path.join(pfs_folder, outputbase + ".pfs")
            with open(pfsoutput, 'wb') as pfs_handle:
                pfs_handle.write(" ".join(domains))

            # Save more complete information of each domain per BGC
            pfdoutput = os.path.join(pfd_folder, outputbase + ".pfd")
            with open(pfdoutput, 'wb') as pfd_handle:
                write_pfd(pfd_handle, filtered_matrix)
        else:
            # there aren't any domains in this BGC
            # delete from all data structures
            print("  No domains where found in " + outputbase + ".domtable. Removing it from further analysis")
            info = genbankDict.get(outputbase)
            clusters.remove(outputbase)
            baseNames.remove(outputbase)
            gbk_files.remove(info[0])
            for sample in info[1]:
                sampleDict[sample].remove(outputbase)
            del genbankDict[outputbase]

    else:
        sys.exit("Error: hmmscan file " + outputbase + " was not found! (parseHmmScan)")

    return ("")


def launch_hmmalign(cores, domains):
    """
    Launches instances of hmmalign with multiprocessing.
    Note that the domains parameter contains the .fasta extension
    """
    pool = Pool(cores, maxtasksperchild=32)
    pool.map(run_hmmalign, domains)
    pool.close()
    pool.join()


def run_hmmalign(domain):
    # domain already contains the full path, with the file extension
    domain_base = domain.split(os.sep)[-1][:-6]
    hmmfetch_pars = ["hmmfetch", os.path.join(pfam_dir, "Pfam-A.hmm.h3m"), domain_base]
    proc_hmmfetch = subprocess.Popen(hmmfetch_pars, stdout=subprocess.PIPE, shell=False)

    hmmalign_pars = ["hmmalign", "-o", domain.replace(".fasta", ".stk"), "-", domain]
    proc_hmmalign = subprocess.Popen(hmmalign_pars, stdin=proc_hmmfetch.stdout, stdout=subprocess.PIPE, shell=False)

    proc_hmmfetch.stdout.close()
    proc_hmmalign.communicate()[0]
    proc_hmmfetch.wait()

    if verbose:
        print(" ".join(hmmfetch_pars) + " | " + " ".join(hmmalign_pars))
    stk_to_algn([domain[:-6] + ".stk"])


def stk_to_algn(stkFiles):
    for stkFile in stkFiles:
        algnFile = stkFile[:-3] + 'algn'
        if not os.path.isfile(algnFile):
            with open(algnFile, 'w') as outfile, open(stkFile, 'r') as infile:
                algnDict = {}
                for line in infile:
                    if line.startswith("#"):
                        continue
                    elif line[0].isupper() or line[0].isdigit() or line[0].islower():
                        header = line.split(' ')[0]
                        algn = line.split(' ')[-1]
                        algn = ''.join([pos for pos in algn if (pos.isupper() or pos == '-')])
                        if header in algnDict.keys():
                            algnDict[header] += algn
                        else:
                            algnDict[header] = ">%s\n%s" % (header, algn)
                outfile.write("\n".join(algnDict.values()))
    return


class FloatRange(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def __eq__(self, other):
        return self.start <= other <= self.end

    def __repr__(self):
        return '{0}-{1}'.format(self.start, self.end)


class bgc_data:
    def __init__(self, accession_id, description, product, records, max_width, biosynthetic_genes, contig_edge):
        # These two properties come from the genbank file:
        self.accession_id = accession_id
        self.description = description
        # AntiSMASH predicted class of compound:
        self.product = product
        # number of records in the genbank file (think of multi-locus BGCs):
        self.records = records
        # length of largest record (it will be used for ArrowerSVG):
        self.max_width = int(max_width)
        # Internal set of tags corresponding to genes that AntiSMASH marked
        # as "Kind: Biosynthetic". It is formed as
        # clusterName + "_ORF" + cds_number + ":gid:" + gene_id + ":pid:" + protein_id + ":loc:" + gene_start + ":" + gene_end + ":strand:" + {+,-}
        self.biosynthetic_genes = biosynthetic_genes
        # AntiSMASH 4+ marks BGCs that sit on the edge of a contig
        self.contig_edge = contig_edge


if __name__ == "__main__":
    options = CMD_parser()

    if options.outputdir == "":
        print "please provide a name for an output folder using parameter -o or --outputdir"
        sys.exit(0)

    global anchor_domains
    if os.path.isfile(options.anchorfile):
        anchor_domains = get_anchor_domains(options.anchorfile)
    else:
        print("File with list of anchor domains not found")
        anchor_domains = set()

    global bgc_class_weight
    global AlignedDomainSequences
    global DomainList
    global verbose
    global BGCs

    # contains the type of the final product of the BGC (as predicted by AntiSMASH),
    # as well as the definition line from the BGC file. Used in the final network files.
    global output_folder

    global pfam_dir
    global timings_file
    global cores
    global local
    global clusterNames, bgcClassNames

    include_singletons = options.include_singletons

    cores = int(options.cores)

    options_all = not options.no_all
    options_samples = options.samples

    options_mix = options.mix
    options_classify = not options.no_classify
    local = options.local

    cutoff_list = options.cutoffs
    for c in cutoff_list:
        if c <= 0.0 or c > 1.0:
            cutoff_list.remove(c)

    output_folder = str(options.outputdir)

    pfam_dir = str(options.pfam_dir)
    h3f = os.path.join(pfam_dir, "Pfam-A.hmm.h3f")
    h3i = os.path.join(pfam_dir, "Pfam-A.hmm.h3i")
    h3m = os.path.join(pfam_dir, "Pfam-A.hmm.h3m")
    h3p = os.path.join(pfam_dir, "Pfam-A.hmm.h3p")
    if not (os.path.isfile(h3f) and os.path.isfile(h3i) and os.path.isfile(h3m) and os.path.isfile(h3p)):
        print("One or more of the necessary Pfam files (.h3f, .h3i, .h3m, .h3p) were not found")
        if os.path.isfile(os.path.join(pfam_dir, "Pfam-A.hmm")):
            print("Please use hmmpress with Pfam-A.hmm")
        else:
            print("Please download the latest Pfam-A.hmm file from http://pfam.xfam.org/")
            print("Then use hmmpress on it, and use the --pfam_dir parameter to point to the location of the files")

    verbose = options.verbose

    networks_folder_all = "networks_all"
    networks_folder_samples = "networks_samples"
    if options.hybrids:
        networks_folder_all += "_hybrids"
        networks_folder_samples += "_hybrids"
    if local:
        networks_folder_all += "_local"
        networks_folder_samples += "_local"

    if options.skip_all and options.skip_ma:
        print("Overriding --skip_ma with --skip_all parameter")
        options.skip_hmmscan = False
        options.skip_ma = False

    time1 = time.time()

    # Make the following available for possibly deleting entries within parseHmmScan
    global genbankDict, gbk_files, sampleDict, clusters, baseNames

    ### Step 1: Get all the input files. Write extract sequence and write fasta if necessary
    print("\n\n   - - Processing input files - -")

    create_directory(output_folder, "Output", False)
    write_parameters(output_folder, options)
    bgc_fasta_folder = os.path.join(output_folder, "fasta")
    create_directory(bgc_fasta_folder, "BGC fastas", False)

    # genbankDict: {cluster_name:[genbank_path_to_1st_instance,[sample_1,sample_2,...]]}
    bgc_info = {}  # Stores, per BGC: predicted type, gbk Description, number of records, width of longest record, GenBank's accession
    inputdir =  options.inputdir
#    inputdir = filter_gbk_files(options.inputdir)
    if not os.path.isfile(output_folder + 'genbankdict.p'):
        genbankDict = get_gbk_files(inputdir, bgc_fasta_folder, int(options.min_bgc_size), options.exclude_gbk_str,
                                    bgc_info)
        # !/usr/bin/env python

        """
        PI: Marnix Medema               marnix.medema@wur.nl

        Developers:
        Jorge Navarro                   jorge.navarromunoz@wur.nl
        Emmanuel (Emzo) de los Santos   E.De-Los-Santos@warwick.ac.uk
        Marley Yeong                    marleyyeong@live.nl
        Vittorio Tracanna               vittorio.tracanna@wur.nl


        Dependencies: hmmer, biopython, (mafft), munkres.py

        See more info on
        https://git.wageningenur.nl/medema-group/biosyntheticSpadesRankingPipeline

        # License: GNU Affero General Public License v3 or later
        # A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.
        """

        import cPickle as pickle
        import os
        import subprocess
        import sys
        import time
        from glob import glob
        from collections import defaultdict
        from multiprocessing import Pool, cpu_count
        from argparse import ArgumentParser

        from Bio import SeqIO
        from functions import *
        from ArrowerSVG import *


        def CMD_parser():
            parser = ArgumentParser()

            parser.add_argument("-o", "--outputdir", dest="outputdir", default="", required=True,
                                help="Output directory, this will contain your pfd, pfs, network and hmmscan output files.")
            parser.add_argument("-i", "--inputdir", dest="inputdir",
                                default=os.path.dirname(os.path.realpath(__file__)),
                                help="Input directory of gbk files, if left empty, all gbk files in current and lower directories will be used.")
            parser.add_argument("-c", "--cores", dest="cores", default=cpu_count(),
                                help="Set the number of cores the script may use (default: use all available cores)")
            parser.add_argument("-v", "--verbose", dest="verbose", action="store_true", default=False,
                                help="Prints more detailed information. Toggle to true.")
            parser.add_argument("--include_singletons", dest="include_singletons", action="store_true", default=False,
                                help="Include nodes that have no edges to other nodes from the network. Toggle to activate.")
            parser.add_argument("-d", "--domain_overlap_cutoff", dest="domain_overlap_cutoff", default=0.1,
                                help="Specify at which overlap percentage domains are considered to overlap.")
            parser.add_argument("-m", "--min_bgc_size", dest="min_bgc_size", default=0,
                                help="Provide the minimum size of a BGC to be included in the analysis. Default is 0 base pairs")

            parser.add_argument("-s", "--samples", dest="samples", action="store_true", default=False,
                                help="Separate the input files into samples according to their containing folder within the input folder. Toggle to activate")

            parser.add_argument("--no_all", dest="no_all", action="store_true", default=False,
                                help="By default, BiG-SCAPE uses a single data set comprised of all input files available recursively within the input folder. Toggle to disactivate this behaviour (in that case, if the --samples parameter is not activated, BiG-SCAPE will not create any network file)")

            parser.add_argument("--mix", dest="mix", action="store_true", default=False,
                                help="By default, BiG-SCAPE separates the analysis according to the BGC product (PKS Type I, NRPS, RiPPs, etc.) and will create network directories for each class. Toggle to include an analysis mixing all classes")

            parser.add_argument("--cluster_family", dest="cluster_family", action="store_true", default=False,
                                help="BiG-SCAPE will perform a second layer of clustering and attempt to group families assigned from clustering with cutoff of 0.5 to clans")

            parser.add_argument("--clan_cutoff", dest="clan_cutoff", default=0.5,
                                help="Distance Cutoff to use for Family Clustering")

            parser.add_argument("--hybrids", dest="hybrids", action="store_true",
                                default=False, help="Toggle to also add BGCs with hybrid\
                                predicted products from the PKS/NRPS Hybrids and Others\
                                classes to each subclass (e.g. a 'terpene-nrps' BGC from\
                                Others would be added to the Terpene and NRPS classes")

            parser.add_argument("--local", dest="local", action="store_true", default=False,
                                help="Activate local mode. BiG-SCAPE will change the logic in the distance calculation phase to try to perform local alignments of shorter, 'fragmented' BGCs by finding the maximum overlap in domain content.")

            parser.add_argument("--no_classify", dest="no_classify", action="store_true", default=False,
                                help="By default, BiG-SCAPE classifies the output files analysis based on the BGC product. Toggle to deactivate (note that if the --mix parameter is not activated, BiG-SCAPE will not create any network file).")

            parser.add_argument("--banned_classes", nargs='+', dest="banned_classes", default=[],
                                choices=["PKSI", "PKSother", "NRPS", "RiPPs", "Saccharides", "Terpene",
                                         "PKS-NRP_Hybrids",
                                         "Others"],
                                help="Classes that should NOT be included in the classification. E.g. \"--banned_classes PKSI PKSOther\"")

            parser.add_argument("--pfam_dir", dest="pfam_dir",
                                default=os.path.dirname('/mnt/scratch/traca001/tools/BiG-SCAPE/'),
                                help="Location of hmmpress-processed Pfam files. Default is same location of BiG-SCAPE")
            parser.add_argument("--anchorfile", dest="anchorfile", default="anchor_domains.txt",
                                help="Provide a custom location for the anchor domains file, default is anchor_domains.txt.")
            parser.add_argument("--exclude_gbk_str", dest="exclude_gbk_str", default="",
                                help="If this string occurs in the gbk filename, this file will not be used for the analysis.")

            parser.add_argument("--mafft_pars", dest="mafft_pars", default="",
                                help="Add single/multiple parameters for MAFFT specific enclosed by quotation marks e.g. \"--nofft --parttree\"")
            parser.add_argument("--al_method", dest="al_method", default="--retree 2",
                                help="alignment method for MAFFT, if there's a space in the method's name, enclose by quotation marks. default: \"--retree 2\" corresponds to the FFT-NS-2 method")
            parser.add_argument("--maxiterate", dest="maxit", default=1000,
                                help="Maxiterate parameter in MAFFT, default is 1000, corresponds to the FFT-NS-2 method")
            parser.add_argument("--mafft_threads", dest="mafft_threads", default=0,
                                help="Set the number of threads in MAFFT, -1 sets the number of threads as the number of physical cores. Default: same as --cores parameter")
            parser.add_argument("--use_mafft", dest="use_mafft", action="store_true", default=False,
                                help="Use MAFFT instead of hmmalign for multiple alignment of domain sequences")
            parser.add_argument("--force_hmmscan", dest="force_hmmscan", action="store_true", default=False,
                                help="Force domain prediction using hmmscan even if BiG-SCAPE finds processed domtable files (e.g. to use a new version of PFAM).")
            parser.add_argument("--skip_ma", dest="skip_ma", action="store_true", default=False,
                                help="Skip multiple alignment of domains' sequences. Use if alignments have been generated in a previous run.")
            parser.add_argument("--skip_all", dest="skip_all", action="store_true",
                                default=False, help="Only generate new network files. ")
            parser.add_argument("--cutoffs", dest="cutoffs", nargs="+",
                                default=[0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50, 0.55, 0.60, 0.65, 0.70,
                                         0.75,
                                         0.80], type=float, choices=[FloatRange(0.0, 1.0)],
                                help="Generate networks using multiple raw distance cutoff values, example: \"0.1, 0.25, 0.5, 1.0\". Default: all values from 0.10 to 0.80 with 0.05 intervals.")

            options = parser.parse_args()
            return options


        def filter_gbk_files(inputdir):
            """Checks all gbk files to see if they belong to nrps, pks or transpks
            :return: list of files to copy in new folder
            """
            products_filter = set(['nrps', 't1pks', 'transatpks'])
            current_dir = ""
            newdirpath = '/'.join(inputdir.split('/')[:-1]) + '_filtered/' + inputdir.split('/')[-1]
            if not os.path.exists(newdirpath):
                os.mkdir(newdirpath)
            for dirpath, dirnames, filenames in os.walk(inputdir):
                head, tail = os.path.split(dirpath)
                if current_dir != tail:
                    current_dir = tail
                genbankfilelist = []
                for fname in filenames:
                    clusterName = fname[:-4]
                    if not fname.endswith('.final'):
                        records = list(SeqIO.parse(os.path.join(dirpath, fname), "genbank"))
                    else:
                        records = list()
                    for record in records:
                        for feature in record.features:
                            if feature.type == "cluster":
                                if len(set(feature.qualifiers["product"][0].split('-')).intersection(
                                        products_filter)) > 0:
                                    copy_file(dirpath, dirnames, fname, newdirpath)
            return newdirpath


        def copy_file(dirpath, dirnames, fname, newdirpath):
            if not os.path.isfile(newdirpath + fname):
                cp_cmd = "cp " + dirpath + '/' + fname + " " + newdirpath + fname
                os.system(cp_cmd)


        def get_gbk_files(inputdir, bgc_fasta_folder, min_bgc_size, exclude_gbk_str, bgc_info):
            """Searches given directory for genbank files recursively, will assume that
            the genbank files that have the same name are the same genbank file. 
            Returns a dictionary that contains the names of the clusters found as keys
            and a list that contains [0] a path to the genbank file and [1] the 
            samples that the genbank file is a part of.
            Extract and write the sequences as fasta files if not already in the Fasta 
            folder.
            return: {cluster_name:[genbank_path,[s_a,s_b...]]}
            """
            genbankDict = {}

            file_counter = 0
            processed_sequences = 0
            biosynthetic_genes = set()
            product_list_per_record = []
            fasta_data = []
            save_fasta = True
            contig_edge = False
            products_filter = set(['nrps', 't1pks', 'transatpks'])
            print("\nImporting GenBank files")
            if exclude_gbk_str != "":
                print(" Skipping files with '" + exclude_gbk_str + "' in their filename")

            current_dir = ""
            for dirpath, dirnames, filenames in os.walk(inputdir):
                print (inputdir)
                print (dirpath)
                print (dirnames)
                print (filenames)
                head, tail = os.path.split(dirpath)

                if current_dir != tail:
                    current_dir = tail

                genbankfilelist = []

                filenames = [x for x in filenames if not x.endswith('.final.gbk')]

                for fname in filenames:
                    if fname[-3:] != "gbk":
                        continue

                    clusterName = fname[:-4]

                    if exclude_gbk_str != "" and exclude_gbk_str in fname:
                        print(" Skipping file " + fname)
                        continue
                    if "_ORF" in fname:
                        print(" Skipping file " + fname + " (string '_ORF' is used internally)")
                        continue

                    if " " in fname:
                        sys.exit(
                            "\nError: Input GenBank files should not have spaces in their filenames as HMMscan cannot process them properly ('too many arguments').")

                    # See if we need to write down the sequence
                    outputfile = os.path.join(bgc_fasta_folder, clusterName + '.fasta')
                    if os.path.isfile(outputfile) and os.path.getsize(outputfile) > 0:
                        save_fasta = False
                    else:
                        save_fasta = True

                    try:
                        # basic file verification. Substitutes check_data_integrity
                        records = list(SeqIO.parse(os.path.join(dirpath, fname), "genbank"))
                    except ValueError as e:
                        print("   Error with file " + os.path.join(dirpath, fname) + ": \n    '" + str(e) + "'")
                        print("    (This file will be excluded from the analysis)")
                        continue
                    else:
                        bgc_size = 0
                        cds_ctr = 0
                        product = "no type"
                        del product_list_per_record[:]

                        max_width = 0  # This will be used for the SVG figure
                        record_count = 0

                        for record in records:
                            skip = False
                            record_count += 1
                            bgc_size += len(record.seq)
                            if len(record.seq) > max_width:
                                max_width = len(record.seq)

                            for feature in record.features:
                                if "cluster" in feature.type and "product" in feature.qualifiers:
                                    if len(feature.qualifiers["product"]) > 1:
                                        print(
                                            "  WARNING: more than product annotated in record " + str(
                                                record_count) + ", " + fname)
                                        break
                                    else:
                                        product_list_per_record.append(
                                            feature.qualifiers["product"][0].replace(" ", ""))

                                # Get biosynthetic genes + sequences
                                if feature.type == "CDS":
                                    cds_ctr += 1

                                    CDS = feature
                                    gene_id = ""
                                    if "gene" in CDS.qualifiers:
                                        # In principle, we should keep a list of genes with
                                        # the same id (isoforms) and only keep the largest
                                        # TODO
                                        gene_id = CDS.qualifiers.get('gene', "")[0]

                                    protein_id = ""
                                    if "protein_id" in CDS.qualifiers:
                                        protein_id = CDS.qualifiers.get('protein_id', "")[0]

                                    # nofuzzy_start/nofuzzy_end are obsolete
                                    # http://biopython.org/DIST/docs/api/Bio.SeqFeature.FeatureLocation-class.html#nofuzzy_start
                                    gene_start = max(0, int(CDS.location.start))
                                    gene_end = max(0, int(CDS.location.end))
                                    direction = CDS.location.strand

                                    if direction == 1:
                                        strand = '+'
                                    else:
                                        strand = '-'

                                    fasta_header = clusterName + "_ORF" + str(cds_ctr) + ":gid:" + str(
                                        gene_id) + ":pid:" + str(
                                        protein_id) + ":loc:" + str(gene_start) + ":" + str(
                                        gene_end) + ":strand:" + strand
                                    fasta_header = fasta_header.replace(">",
                                                                        "")  # the coordinates might contain larger than signs, tools upstream don't like this
                                    fasta_header = fasta_header.replace(" ",
                                                                        "")  # the domtable output format (hmmscan) uses spaces as a delimiter, so these cannot be present in the fasta header

                                    if "sec_met" in feature.qualifiers:
                                        if "Kind: biosynthetic" in feature.qualifiers["sec_met"]:
                                            biosynthetic_genes.add(fasta_header)

                                    fasta_header = ">" + fasta_header

                                    if save_fasta:
                                        if 'translation' in CDS.qualifiers.keys():
                                            prot_seq = CDS.qualifiers['translation'][0]
                                        # If translation isn't available translate manually, this will take longer
                                        else:
                                            nt_seq = CDS.location.extract(record.seq)

                                            # If we know sequence is an ORF (like all CDSs), codon table can be
                                            #  used to correctly translate alternative start codons.
                                            #  see http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc25
                                            # If the sequence has a fuzzy start/end, it might not be complete,
                                            # (therefore it might not be the true start codon)
                                            # However, in this case, if 'translation' not available, assume
                                            #  this is just a random sequence
                                            complete_cds = False

                                            # More about fuzzy positions
                                            # http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc39
                                            fuzzy_start = False
                                            if str(CDS.location.start)[0] in "<>":
                                                complete_cds = False
                                                fuzzy_start = True

                                            fuzzy_end = False
                                            if str(CDS.location.end)[0] in "<>":
                                                fuzzy_end = True

                                            # for protein sequence if it is at the start of the entry assume
                                            # that end of sequence is in frame and trim from the beginning
                                            # if it is at the end of the genbank entry assume that the start
                                            # of the sequence is in frame
                                            reminder = len(nt_seq) % 3
                                            if reminder > 0:
                                                if fuzzy_start and fuzzy_end:
                                                    print(
                                                        "Warning, CDS (" + clusterName + ", " +
                                                        CDS.qualifiers.get('locus_tag', "")[
                                                            0] + ") has fuzzy start and end positions, and a sequence length not multiple of three. Skipping")
                                                    break

                                                if fuzzy_start:
                                                    if reminder == 1:
                                                        nt_seq = nt_seq[1:]
                                                    else:
                                                        nt_seq = nt_seq[2:]
                                                # fuzzy end
                                                else:
                                                    # same logic reverse direction
                                                    if reminder == 1:
                                                        nt_seq = nt_seq[:-1]
                                                    else:
                                                        nt_seq = nt_seq[:-2]

                                            # The Genetic Codes: www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi
                                            if "transl_table" in CDS.qualifiers.keys():
                                                CDStable = CDS.qualifiers.get("transl_table", "")[0]
                                                prot_seq = str(
                                                    nt_seq.translate(table=CDStable, to_stop=True, cds=complete_cds))
                                            else:
                                                prot_seq = str(nt_seq.translate(to_stop=True, cds=complete_cds))

                                        fasta_data.append((fasta_header, prot_seq))

                        if bgc_size > min_bgc_size:  # exclude the bgc if it's too small
                            file_counter += 1
                            # check what we have product-wise
                            # In particular, handle different products for multi-record files
                            product_set = set(product_list_per_record)
                            if len(product_set) == 1:  # only one type of product
                                product = product_list_per_record[0]
                            elif "other" in product_set:  # more than one, and it contains "other"
                                if len(product_set) == 2:
                                    product = list(product_set - set(['other']))[0]  # product = not "other"
                                else:
                                    product = "-".join(product_set - set(['other']))  # likely a hybrid
                            else:
                                product = "-".join(product_set)  # likely a hybrid

                            # assuming that the definition field is the same in all records
                            # product: antiSMASH predicted class of metabolite
                            # gbk definition
                            # number of records (for Arrower figures)
                            # max_width: width of the largest record (for Arrower figures)
                            # id: the GenBank's accession
                            # bgc_info[clusterName] = (product, records[0].description, len(records), max_width, records[0].id, biosynthetic_genes.copy())
                            # TODO contig_edge annotation is not present for antiSMASH v < 4
                            # Perhaps we can try to infer if it's in a contig edge: if
                            # - first biosynthetic gene start < 10kb or
                            # - max_width - last biosynthetic gene end < 10kb (but this will work only for the largest record)
                            bgc_info[clusterName] = bgc_data(records[0].id, records[0].description, product,
                                                             len(records),
                                                             max_width, biosynthetic_genes.copy(), contig_edge)

                            if len(bgc_info[clusterName].biosynthetic_genes) == 0:
                                print("  Warning: No Biosynthetic Genes found in " + clusterName)

                            # TODO why re-process everything if it was already in the list?
                            # if name already in genbankDict.keys -> add current_dir
                            # else: extract all info
                            if clusterName in genbankDict.keys():
                                # Name was already in use. Use current_dir as the new sample's name
                                genbankDict[clusterName][1].add(current_dir)
                            else:
                                # location of first instance of the file is genbankDict[clustername][0]
                                genbankDict.setdefault(clusterName, [os.path.join(dirpath, fname), set([current_dir])])

                                # See if we need to write down the sequence
                                outputfile = os.path.join(bgc_fasta_folder, clusterName + '.fasta')
                                if os.path.isfile(outputfile) and os.path.getsize(outputfile) > 0:
                                    processed_sequences += 1
                                else:
                                    with open(outputfile, 'w') as fastaHandle:
                                        for header_sequence in fasta_data:
                                            fastaHandle.write('%s\n' % header_sequence[0])
                                            fastaHandle.write('%s\n' % header_sequence[1])

                            if verbose:
                                print("  Adding " + fname + " (" + str(bgc_size) + " bps)")

                        else:
                            print(
                            " Discarding " + clusterName + " (size less than " + str(min_bgc_size) + " bp, was " + str(
                                bgc_size) + ")")

                        del fasta_data[:]
                        biosynthetic_genes.clear()

            if file_counter == 0:
                sys.exit("\nError: There are no files to process")

            # if file_counter == 1:
            #    sys.exit("\nError: Only one file found. Please input at least two files")

            print("\n Starting with " + str(file_counter) + " files")
            print(" Files that had its sequence extracted: " + str(file_counter - processed_sequences))
            if not os.path.isfile(output_folder + 'bgc_info.p'):
                pickle.dump(bgc_info, open(output_folder + 'bgc_info.p', "w"))
            return genbankDict


        def runHmmScan(fastaPath, hmmPath, outputdir, verbose):
            """ Runs hmmscan command on a fasta file with a single core to generate a
            domtable file"""
            hmmFile = os.path.join(hmmPath, "Pfam-A.hmm")
            if os.path.isfile(fastaPath):
                name = ".".join(fastaPath.split(os.sep)[-1].split(".")[:-1])
                outputName = os.path.join(outputdir, name + ".domtable")

                hmmscan_cmd = "hmmscan --cpu 0 --domtblout %s --cut_tc %s %s" % (outputName, hmmFile, fastaPath)
                if verbose == True:
                    print("   " + hmmscan_cmd)
                subprocess.check_output(hmmscan_cmd, shell=True)

            else:
                sys.exit("Error running hmmscan: Fasta file " + fastaPath + " doesn't exist")


        def parseHmmScan(hmmscanResults, pfd_folder, pfs_folder, overlapCutoff):
            outputbase = ".".join(hmmscanResults.split(os.sep)[-1].split(".")[:-1])
            # try to read the domtable file to find out if this gbk has domains. Domains
            # need to be parsed into fastas anyway.
            if os.path.isfile(hmmscanResults):
                pfd_matrix = domtable_parser(outputbase, hmmscanResults)

                # get number of domains to decide if this BGC should be removed
                num_domains = len(pfd_matrix)

                if num_domains > 0:
                    print("  Processing domtable file: " + outputbase)

                    # check_overlap also sorts the filtered_matrix results and removes
                    # overlapping domains, keeping the highest scoring one
                    filtered_matrix, domains = check_overlap(pfd_matrix, overlapCutoff)

                    # Save list of domains per BGC
                    pfsoutput = os.path.join(pfs_folder, outputbase + ".pfs")
                    with open(pfsoutput, 'wb') as pfs_handle:
                        pfs_handle.write(" ".join(domains))

                    # Save more complete information of each domain per BGC
                    pfdoutput = os.path.join(pfd_folder, outputbase + ".pfd")
                    with open(pfdoutput, 'wb') as pfd_handle:
                        write_pfd(pfd_handle, filtered_matrix)
                else:
                    # there aren't any domains in this BGC
                    # delete from all data structures
                    print("  No domains where found in " + outputbase + ".domtable. Removing it from further analysis")
                    info = genbankDict.get(outputbase)
                    clusters.remove(outputbase)
                    baseNames.remove(outputbase)
                    gbk_files.remove(info[0])
                    for sample in info[1]:
                        sampleDict[sample].remove(outputbase)
                    del genbankDict[outputbase]

            else:
                sys.exit("Error: hmmscan file " + outputbase + " was not found! (parseHmmScan)")

            return ("")


        def launch_hmmalign(cores, domains):
            """
            Launches instances of hmmalign with multiprocessing.
            Note that the domains parameter contains the .fasta extension
            """
            pool = Pool(cores, maxtasksperchild=32)
            pool.map(run_hmmalign, domains)
            pool.close()
            pool.join()


        def run_hmmalign(domain):
            # domain already contains the full path, with the file extension
            domain_base = domain.split(os.sep)[-1][:-6]
            hmmfetch_pars = ["hmmfetch", os.path.join(pfam_dir, "Pfam-A.hmm.h3m"), domain_base]
            proc_hmmfetch = subprocess.Popen(hmmfetch_pars, stdout=subprocess.PIPE, shell=False)

            hmmalign_pars = ["hmmalign", "-o", domain.replace(".fasta", ".stk"), "-", domain]
            proc_hmmalign = subprocess.Popen(hmmalign_pars, stdin=proc_hmmfetch.stdout, stdout=subprocess.PIPE,
                                             shell=False)

            proc_hmmfetch.stdout.close()
            proc_hmmalign.communicate()[0]
            proc_hmmfetch.wait()

            if verbose:
                print(" ".join(hmmfetch_pars) + " | " + " ".join(hmmalign_pars))
            stk_to_algn([domain[:-6] + ".stk"])


        def stk_to_algn(stkFiles):
            for stkFile in stkFiles:
                algnFile = stkFile[:-3] + 'algn'
                if not os.path.isfile(algnFile):
                    with open(algnFile, 'w') as outfile, open(stkFile, 'r') as infile:
                        algnDict = {}
                        for line in infile:
                            if line.startswith("#"):
                                continue
                            elif line[0].isupper() or line[0].isdigit() or line[0].islower():
                                header = line.split(' ')[0]
                                algn = line.split(' ')[-1]
                                algn = ''.join([pos for pos in algn if (pos.isupper() or pos == '-')])
                                if header in algnDict.keys():
                                    algnDict[header] += algn
                                else:
                                    algnDict[header] = ">%s\n%s" % (header, algn)
                        outfile.write("\n".join(algnDict.values()))
            return


        class FloatRange(object):
            def __init__(self, start, end):
                self.start = start
                self.end = end

            def __eq__(self, other):
                return self.start <= other <= self.end

            def __repr__(self):
                return '{0}-{1}'.format(self.start, self.end)


        class bgc_data:
            def __init__(self, accession_id, description, product, records, max_width, biosynthetic_genes, contig_edge):
                # These two properties come from the genbank file:
                self.accession_id = accession_id
                self.description = description
                # AntiSMASH predicted class of compound:
                self.product = product
                # number of records in the genbank file (think of multi-locus BGCs):
                self.records = records
                # length of largest record (it will be used for ArrowerSVG):
                self.max_width = int(max_width)
                # Internal set of tags corresponding to genes that AntiSMASH marked
                # as "Kind: Biosynthetic". It is formed as
                # clusterName + "_ORF" + cds_number + ":gid:" + gene_id + ":pid:" + protein_id + ":loc:" + gene_start + ":" + gene_end + ":strand:" + {+,-}
                self.biosynthetic_genes = biosynthetic_genes
                # AntiSMASH 4+ marks BGCs that sit on the edge of a contig
                self.contig_edge = contig_edge


        if __name__ == "__main__":
            options = CMD_parser()

            if options.outputdir == "":
                print "please provide a name for an output folder using parameter -o or --outputdir"
                sys.exit(0)

            global anchor_domains
            if os.path.isfile(options.anchorfile):
                anchor_domains = get_anchor_domains(options.anchorfile)
            else:
                print("File with list of anchor domains not found")
                anchor_domains = set()

            global bgc_class_weight
            global AlignedDomainSequences
            global DomainList
            global verbose
            global BGCs

            # contains the type of the final product of the BGC (as predicted by AntiSMASH),
            # as well as the definition line from the BGC file. Used in the final network files.
            global output_folder

            global pfam_dir
            global timings_file
            global cores
            global local
            global clusterNames, bgcClassNames

            include_singletons = options.include_singletons

            cores = int(options.cores)

            options_all = not options.no_all
            options_samples = options.samples

            options_mix = options.mix
            options_classify = not options.no_classify
            local = options.local

            cutoff_list = options.cutoffs
            for c in cutoff_list:
                if c <= 0.0 or c > 1.0:
                    cutoff_list.remove(c)

            output_folder = str(options.outputdir)

            pfam_dir = str(options.pfam_dir)
            h3f = os.path.join(pfam_dir, "Pfam-A.hmm.h3f")
            h3i = os.path.join(pfam_dir, "Pfam-A.hmm.h3i")
            h3m = os.path.join(pfam_dir, "Pfam-A.hmm.h3m")
            h3p = os.path.join(pfam_dir, "Pfam-A.hmm.h3p")
            if not (os.path.isfile(h3f) and os.path.isfile(h3i) and os.path.isfile(h3m) and os.path.isfile(h3p)):
                print("One or more of the necessary Pfam files (.h3f, .h3i, .h3m, .h3p) were not found")
                if os.path.isfile(os.path.join(pfam_dir, "Pfam-A.hmm")):
                    print("Please use hmmpress with Pfam-A.hmm")
                else:
                    print("Please download the latest Pfam-A.hmm file from http://pfam.xfam.org/")
                    print(
                    "Then use hmmpress on it, and use the --pfam_dir parameter to point to the location of the files")

            verbose = options.verbose

            networks_folder_all = "networks_all"
            networks_folder_samples = "networks_samples"
            if options.hybrids:
                networks_folder_all += "_hybrids"
                networks_folder_samples += "_hybrids"
            if local:
                networks_folder_all += "_local"
                networks_folder_samples += "_local"

            if options.skip_all and options.skip_ma:
                print("Overriding --skip_ma with --skip_all parameter")
                options.skip_hmmscan = False
                options.skip_ma = False

            time1 = time.time()

            # Make the following available for possibly deleting entries within parseHmmScan
            global genbankDict, gbk_files, sampleDict, clusters, baseNames

            ### Step 1: Get all the input files. Write extract sequence and write fasta if necessary
            print("\n\n   - - Processing input files - -")

            create_directory(output_folder, "Output", False)
            write_parameters(output_folder, options)
            bgc_fasta_folder = os.path.join(output_folder, "fasta")
            create_directory(bgc_fasta_folder, "BGC fastas", False)

            # genbankDict: {cluster_name:[genbank_path_to_1st_instance,[sample_1,sample_2,...]]}
            bgc_info = {}  # Stores, per BGC: predicted type, gbk Description, number of records, width of longest record, GenBank's accession
            inputdir = options.inputdir
            #    inputdir = filter_gbk_files(options.inputdir)
            if not os.path.isfile(output_folder + 'genbankdict.p'):
                genbankDict = get_gbk_files(inputdir, bgc_fasta_folder, int(options.min_bgc_size),
                                            options.exclude_gbk_str,
                                            bgc_info)
                print output_folder + 'genbankdict.p'
                pickle.dump(genbankDict, open(output_folder + 'genbankdict.p', "w"))

            else:
                print "genbankDict found, loading."
                genbankDict = pickle.load(open(output_folder + 'genbankdict.p', 'r'))
                print "Loading complete"

            # clusters and sampleDict contain the necessary structure for all-vs-all and sample analysis
            clusters = genbankDict.keys()

            sampleDict = {}  # {sampleName:set(bgc1,bgc2,...)}
            gbk_files = []  # raw list of gbk file locations
            for (cluster, (path, clusterSample)) in genbankDict.iteritems():
                gbk_files.append(path)
                for sample in clusterSample:
                    clustersInSample = sampleDict.get(sample, set())
                    clustersInSample.add(cluster)
                    sampleDict[sample] = clustersInSample

            print("\nCreating output directories")

            domtable_folder = os.path.join(output_folder, "domtable")
            pfs_folder = os.path.join(output_folder, "pfs")
            pfd_folder = os.path.join(output_folder, "pfd")
            domains_folder = os.path.join(output_folder, "domains")
            svg_folder = os.path.join(output_folder, "SVG")

            create_directory(domtable_folder, "Domtable", False)
            create_directory(domains_folder, "Domains", False)
            create_directory(pfs_folder, "pfs", False)
            create_directory(pfd_folder, "pfd", False)
            create_directory(svg_folder, "SVG", False)

            print("\nTrying threading on %i cores" % cores)

            """BGCs -- 
            dictionary of this structure:
            BGCs = {'cluster_name_x': { 'general_domain_name_x' : ['specific_domain_name_1',
             'specific_domain_name_2'] } }
            - cluster_name_x: cluster name (can be anything)
            - general_domain_name_x: PFAM ID, for example 'PF00550'
            - specific_domain_name_x: ID of a specific domain that will allow to you to map it to names in DMS unequivocally
             (for example, 'PF00550_start_end', where start and end are genomic positions)."""
            BGCs = {}  # will contain the BGCs

            # Weights in the format J, DSS, AI, anchorboost
            # Generated with optimization results 2016-12-05.
            # Used the basic list of 4 anchor domains.
            bgc_class_weight = {}
            bgc_class_weight["PKSI"] = (0.22, 0.76, 0.02, 1.0)
            bgc_class_weight["PKSother"] = (0.0, 0.32, 0.68, 4.0)
            bgc_class_weight["NRPS"] = (0.0, 0.75, 0.25, 4.0)
            bgc_class_weight["RiPPs"] = (0.28, 0.71, 0.01, 1.0)
            bgc_class_weight["Saccharides"] = (0.0, 0.0, 1.0, 1.0)
            bgc_class_weight["Terpene"] = (0.2, 0.75, 0.05, 2.0)
            bgc_class_weight["PKS-NRP_Hybrids"] = (0.0, 0.78, 0.22, 1.0)
            bgc_class_weight["Others"] = (0.01, 0.97, 0.02, 4.0)
            bgc_class_weight["Custom"] = (0.0, 1.0, 0.0, 4.0)
            # define which classes will be analyzed (if in the options_classify mode)
            valid_classes = set()
            for key in bgc_class_weight:
                valid_classes.add(key.lower())
            user_banned_classes = set([a.strip().lower() for a in options.banned_classes])
            valid_classes = valid_classes - user_banned_classes
            bgc_class_weight["mix"] = (0.2, 0.75, 0.05, 2.0)  # default when not separating in classes
            BGC_classes = defaultdict(list)
            # mix class will always be the last element of the tuple
            bgcClassNames = tuple(sorted(list(bgc_class_weight)) + ["mix"])
            assert bgcClassNames[-1] == 'mix'

            bgcClassName2idx = dict(zip(bgcClassNames, range(len(bgcClassNames))))

            AlignedDomainSequences = {}  # Key: specific domain sequence label. Item: aligned sequence
            DomainList = {}  # Key: BGC. Item: ordered list of domains

            # to avoid multiple alignment if there's only 1 seq. representing a particular domain
            sequences_per_domain = {}
            ### Step 2: Run hmmscan
            print("\nPredicting domains using hmmscan")

            baseNames = set(clusters)

            # All available fasta files (could be more than it should if reusing output folder)
            allFastaFiles = set(glob(os.path.join(bgc_fasta_folder, "*.fasta")))

            # fastaFiles: all the fasta files that should be there
            # (i.e. correspond to the input files)
            fastaFiles = set()
            for name in baseNames:
                fastaFiles.add(os.path.join(bgc_fasta_folder, name + ".fasta"))

            # fastaBases: the actual fasta files we have that correspond to the input
            fastaBases = allFastaFiles.intersection(fastaFiles)

            # Verify that all input files had their fasta sequences extracted
            if len(fastaFiles - fastaBases) > 0:
                sys.exit("Error! The following files did NOT have their fasta sequences extracted: " + ", ".join(
                    fastaFiles - fastaBases))

            # Make a list of all fasta files that need to be processed
            # (i.e., they don't yet have a corresponding .domtable)
            if options.force_hmmscan:
                # process all files, regardless of whether they already existed
                task_set = fastaFiles
                print(" Forcing domain prediction on ALL fasta files (--force_hmmscan)")
            else:
                # find already processed files
                alreadyDone = set()
                for fasta in fastaFiles:
                    outputbase = ".".join(fasta.split(os.sep)[-1].split(".")[:-1])
                    outputfile = os.path.join(domtable_folder, outputbase + '.domtable')
                    if os.path.isfile(outputfile) and os.path.getsize(outputfile) > 0:
                        alreadyDone.add(fasta)

                if len(fastaFiles - alreadyDone) == 0:
                    print(" All fasta files had already been processed")
                elif len(alreadyDone) > 0:
                    if len(fastaFiles - alreadyDone) < 20:
                        print " Warning! The following NEW fasta file(s) will be processed: %s" % ", ".join(
                            ".".join(x.split(os.sep)[-1].split(".")[:-1]) for x in fastaFiles - alreadyDone)
                    else:
                        print(" Warning: " + str(len(fastaFiles - alreadyDone)) + " NEW fasta files will be processed")
                else:
                    print(" Predicting domains for " + str(len(fastaFiles)) + " fasta files")

                task_set = fastaFiles - alreadyDone

            pool = Pool(cores, maxtasksperchild=1)
            for fastaFile in task_set:
                pool.apply_async(runHmmScan, args=(fastaFile, pfam_dir, domtable_folder, verbose))
            pool.close()
            pool.join()

            print " Finished generating domtable files."

            ### Step 3: Parse hmmscan domtable results and generate pfs and pfd files
            print("\nParsing hmmscan domtable files")

            # All available domtable files
            allDomtableFiles = set(glob(os.path.join(domtable_folder, "*.domtable")))

            # domtableFiles: all domtable files corresponding to the input files
            domtableFiles = set()
            for name in baseNames:
                domtableFiles.add(os.path.join(domtable_folder, name + ".domtable"))

            # domtableBases: the actual set of input files with coresponding domtable files
            domtableBases = allDomtableFiles.intersection(domtableFiles)

            # Verify that all input files have a corresponding domtable file
            if len(domtableFiles - domtableBases) > 0:
                sys.exit("Error! The following files did NOT have their domains predicted: " + ", ".join(
                    domtableFiles - domtableBases))

            # find already processed files (assuming that if the pfd file exists, the pfs should too)
            alreadyDone = set()
            if not options.force_hmmscan:
                for domtable in domtableFiles:
                    outputbase = ".".join(domtable.split(os.sep)[-1].split(".")[:-1])
                    outputfile = os.path.join(pfd_folder, outputbase + '.pfd')
                    if os.path.isfile(outputfile) and os.path.getsize(outputfile) > 0:
                        alreadyDone.add(domtable)

            if len(domtableFiles - alreadyDone) == 0:  # Re-run
                print(" All domtable files had already been processed")
            elif len(alreadyDone) > 0:  # Incomplete run
                if len(domtableFiles - alreadyDone) < 20:
                    print " Warning! The following domtable files had not been processed: %s" % ", ".join(
                        ".".join(x.split(os.sep)[-1].split(".")[:-1]) for x in domtableFiles - alreadyDone)
                else:
                    print(" Warning: " + str(len(domtableFiles - alreadyDone)) + " domtable files will be processed")
            else:  # First run
                print(" Processing " + str(len(domtableFiles)) + " domtable files")

            # If using the multiprocessing version and outputbase doesn't have any
            #  predicted domains, it's not as easy to remove if from the analysis
            #  (probably because parseHmmScan only has a copy of clusters et al?)
            # Using serialized version for now. Probably doesn't have too bad an impact
            # pool = Pool(cores,maxtasksperchild=32)
            for domtableFile in domtableFiles - alreadyDone:
                parseHmmScan(domtableFile, pfd_folder, pfs_folder, options.domain_overlap_cutoff)
                # pool.apply_async(parseHmmScan, args=(domtableFile,output_folder,options.domain_overlap_cutoff))
            # pool.close()
            # pool.join()

            # If number of pfd files did not change, no new sequences were added to the
            #  domain fastas and we could try to resume the multiple alignment phase
            # baseNames have been pruned of BGCs with no domains that might've been added temporarily
            try_MA_resume = False
            if len(baseNames - set(pfd.split(os.sep)[-1][:-9] for pfd in alreadyDone)) == 0:
                try_MA_resume = True
            else:
                # new sequences will be added to the domain fasta files. Clean domains folder
                # We could try to make it so it's not necessary to re-calculate every alignment,
                #  either by expanding previous alignment files or at the very least,
                #  re-aligning only the domain files of the newly added BGCs
                print(" New domain sequences to be added; cleaning domains folder")
                for thing in os.listdir(domains_folder):
                    os.remove(os.path.join(domains_folder, thing))

            print " Finished generating generating pfs and pfd files."

            ### Step 4: Parse the pfs, pfd files to generate BGC dictionary, clusters, and clusters per sample objects
            print("\nProcessing domains sequence files")

            # All available pfd files
            allPfdFiles = set(glob(os.path.join(pfd_folder, "*.pfd")))

            # pfdFiles: all pfd files corresponding to the input files
            # (some input files could've been removed due to not having predicted domains)
            pfdFiles = set()
            for name in baseNames:
                pfdFiles.add(os.path.join(pfd_folder, name + ".pfd"))

            # pfdBases: the actual set of input files that have pfd files
            pfdBases = allPfdFiles.intersection(pfdFiles)

            # verify previous step.
            # All BGCs without predicted domains should no longer be in baseNames
            if len(pfdFiles - pfdBases) > 0:
                sys.exit(
                    "Error! The following files did NOT have their domtable files processed: " + ", ".join(
                        pfdFiles - pfdBases))

            filtered_matrix = []
            if options.skip_ma:
                print(" Running with skip_ma parameter: Assuming that the domains folder has all the fasta files")
                try:
                    with open(os.path.join(output_folder + "BGCs.dict"), "r") as BGC_file:
                        BGCs = pickle.load(BGC_file)
                        BGC_file.close()
                except IOError:
                    sys.exit("BGCs file not found...")
            else:
                print(" Adding sequences to corresponding domains file")

                for outputbase in baseNames:
                    if verbose:
                        print("   Processing: " + outputbase)

                    pfdFile = os.path.join(pfd_folder, outputbase + ".pfd")
                    filtered_matrix = [map(lambda x: x.strip(), line.split('\t')) for line in open(pfdFile)]

                    # save each domain sequence from a single BGC in its corresponding file
                    fasta_file = os.path.join(bgc_fasta_folder, outputbase + ".fasta")

                    # only create domain fasta if the pfd content is different from original and
                    #  domains folder has been emptied. Else, if trying to resume alignment phase,
                    #  domain fasta files will contain duplicate sequence labels
                    if not try_MA_resume:
                        with open(fasta_file, "r") as fasta_file_handle:
                            fasta_dict = fasta_parser(fasta_file_handle)  # all fasta info from a BGC
                        save_domain_seqs(filtered_matrix, fasta_dict, domains_folder, outputbase)

                    BGCs[outputbase] = BGC_dic_gen(filtered_matrix)

                    del filtered_matrix[:]

                # store processed BGCs dictionary for future re-runs
                if not os.path.isfile(output_folder + "BGCs.dict"):
                    with open(os.path.join(output_folder, "BGCs.dict"), "w") as BGC_file:
                        pickle.dump(BGCs, BGC_file)
                        BGC_file.close()

            # Get the ordered list of domains
            print(" Reading the ordered list of domains from the pfs files")
            for outputbase in baseNames:
                pfsfile = os.path.join(pfs_folder, outputbase + ".pfs")
                if os.path.isfile(pfsfile):
                    DomainList[outputbase] = get_domain_list(pfsfile)
                else:
                    sys.exit(" Error: could not open " + outputbase + ".pfs")

            ### Step 5: Create SVG figures
            print(" Creating arrower-like figures for each BGC")

            # verify if there are figures already generated

            # All available SVG files
            availableSVGs = set()
            for svg in glob(os.path.join(svg_folder, "*.svg")):
                (root, ext) = os.path.splitext(svg)
                availableSVGs.add(root.split(os.sep)[-1])

            # Which files actually need to be generated
            working_set = baseNames - availableSVGs

            if len(working_set) > 0:
                color_genes = read_color_genes_file()
                color_domains = read_color_domains_file()
                pfam_domain_categories = read_pfam_domain_categories()

                print("  Parsing hmm file for domain names")
                pfam_info = {}
                with open(os.path.join(pfam_dir, "Pfam-A.hmm"), "r") as pfam:
                    putindict = False
                    # assuming that the order of the information never changes
                    for line in pfam:
                        if line[:4] == "NAME":
                            name = line.strip()[6:]
                        if line[:3] == "ACC":
                            acc = line.strip()[6:].split(".")[0]
                        if line[:4] == "DESC":
                            desc = line.strip()[6:]
                            putindict = True

                        if putindict:
                            putindict = False
                            pfam_info[acc] = (name, desc)
                print("    Done")

                # This must be done serially, because if a color for a gene/domain
                # is not found, the text files with colors need to be updated
                print("  Reading BGC information and writing SVG")
                for bgc in working_set:
                    SVG(False, os.path.join(svg_folder, bgc + ".svg"), genbankDict[bgc][0],
                        os.path.join(pfd_folder, bgc + ".pfd"), True, color_genes, color_domains,
                        pfam_domain_categories,
                        pfam_info, bgc_info[bgc].records, bgc_info[bgc].max_width)

                color_genes.clear()
                color_domains.clear()
                pfam_domain_categories.clear()
            elif len(working_set) == 0:
                print("  All SVG from the input files seem to be in the SVG folder")

            availableSVGs.clear()
            print(" Finished creating figures")

            print("\n\n   - - Calculating distance matrix - -")

            # Do multiple alignments if needed
            if not options.skip_ma:
                print("Performing multiple alignment of domain sequences")

                # obtain all fasta files with domain sequences
                fasta_domains = set(glob(os.path.join(domains_folder, "*.fasta")))

                # compare with .algn set of files. Maybe resuming is possible if
                # no new sequences were added
                if try_MA_resume:
                    temp_aligned = set(glob(os.path.join(domains_folder, "*.algn")))

                    if len(temp_aligned) > 0:
                        print(" Found domain fasta files without corresponding alignments")

                        for a in temp_aligned:
                            if os.path.getsize(a) > 0:
                                fasta_domains.remove(a[:-5] + ".fasta")

                    temp_aligned.clear()

                # Try to further reduce the set of domain fastas that need alignment
                sequence_tag_list = set()
                header_list = []
                fasta_domains_temp = fasta_domains.copy()
                for domain_file in fasta_domains_temp:
                    domain_name = ".".join(domain_file.split(os.sep)[-1].split(".")[:-1])

                    # fill fasta_dict...
                    with open(domain_file, "r") as fasta_handle:
                        header_list = get_fasta_keys(fasta_handle)

                    # Get the BGC name from the sequence tag. The form of the tag is:
                    # >BGCXXXXXXX_BGCXXXXXXX_ORF25:gid...
                    sequence_tag_list = set(s.split("_ORF")[0] for s in header_list)

                    # ...to find out how many sequences do we actually have
                    # if len(sequence_tag_list) == 1:
                    # avoid multiple alignment if the domains all belong to the same BGC
                    #    fasta_domains.remove(domain_file)
                    #    if verbose:
                    #        print(" Skipping Multiple Alignment for " + domain_name + " (appears only in one BGC)")

                sequence_tag_list.clear()
                del header_list[:]

                fasta_domains_temp.clear()

                # Do the multiple alignment
                if len(fasta_domains) > 0:
                    print("\n Using hmmalign")
                    launch_hmmalign(cores, fasta_domains)

                    # verify all tasks were completed by checking existance of alignment file
                    for domain in fasta_domains:
                        if not os.path.isfile(domain[:-6] + ".algn"):
                            print(
                            "   WARNING, " + domain[:-6] + ".algn could not be found (possible issue with aligner).")

                else:
                    print(" No domain fasta files found to align")

            # If there's something to analyze, load the aligned sequences
            if options_samples or options_all:
                print(" Trying to read domain alignments (*.algn files)")
                aligned_files_list = glob(os.path.join(domains_folder, "*.algn"))
                if len(aligned_files_list) == 0:
                    sys.exit(
                        "No aligned sequences found in the domain folder (run without the --skip_ma parameter or point to the correct output folder)")
                for aligned_file in aligned_files_list:
                    with open(aligned_file, "r") as aligned_file_handle:
                        fasta_dict = fasta_parser(aligned_file_handle)
                        for header in fasta_dict:
                            AlignedDomainSequences[header] = fasta_dict[header]

            # pickle all needed files:
            if not os.path.isfile(output_folder + "DomainList.p"):
                pickle.dump(DomainList, open(output_folder + "DomainList.p", "w"))
            if not os.path.isfile(output_folder + "AlignedDomainSequences.p"):
                pickle.dump(AlignedDomainSequences, open(output_folder + "AlignedDomainSequences.p", "w"))
            if not os.path.isfile(output_folder + "bgc_info.p"):
                pickle.dump(bgc_info, open(output_folder + 'bgc_info.p', "w"))



        pickle.dump(genbankDict, open(output_folder + 'genbankdict.p', "w"))

    else:
        print "genbankDict found, loading."
        genbankDict = pickle.load(open(output_folder + 'genbankdict.p', 'r'))
        print "Loading complete"

    # clusters and sampleDict contain the necessary structure for all-vs-all and sample analysis
    clusters = genbankDict.keys()

    sampleDict = {}  # {sampleName:set(bgc1,bgc2,...)}
    gbk_files = []  # raw list of gbk file locations
    for (cluster, (path, clusterSample)) in genbankDict.iteritems():
        gbk_files.append(path)
        for sample in clusterSample:
            clustersInSample = sampleDict.get(sample, set())
            clustersInSample.add(cluster)
            sampleDict[sample] = clustersInSample

    print("\nCreating output directories")

    domtable_folder = os.path.join(output_folder, "domtable")
    pfs_folder = os.path.join(output_folder, "pfs")
    pfd_folder = os.path.join(output_folder, "pfd")
    domains_folder = os.path.join(output_folder, "domains")
    svg_folder = os.path.join(output_folder, "SVG")

    create_directory(domtable_folder, "Domtable", False)
    create_directory(domains_folder, "Domains", False)
    create_directory(pfs_folder, "pfs", False)
    create_directory(pfd_folder, "pfd", False)
    create_directory(svg_folder, "SVG", False)

    print("\nTrying threading on %i cores" % cores)

    """BGCs -- 
    dictionary of this structure:
    BGCs = {'cluster_name_x': { 'general_domain_name_x' : ['specific_domain_name_1',
     'specific_domain_name_2'] } }
    - cluster_name_x: cluster name (can be anything)
    - general_domain_name_x: PFAM ID, for example 'PF00550'
    - specific_domain_name_x: ID of a specific domain that will allow to you to map it to names in DMS unequivocally
     (for example, 'PF00550_start_end', where start and end are genomic positions)."""
    BGCs = {}  # will contain the BGCs

    # Weights in the format J, DSS, AI, anchorboost
    # Generated with optimization results 2016-12-05.
    # Used the basic list of 4 anchor domains.
    bgc_class_weight = {}
    bgc_class_weight["PKSI"] = (0.22, 0.76, 0.02, 1.0)
    bgc_class_weight["PKSother"] = (0.0, 0.32, 0.68, 4.0)
    bgc_class_weight["NRPS"] = (0.0, 0.75, 0.25, 4.0)
    bgc_class_weight["RiPPs"] = (0.28, 0.71, 0.01, 1.0)
    bgc_class_weight["Saccharides"] = (0.0, 0.0, 1.0, 1.0)
    bgc_class_weight["Terpene"] = (0.2, 0.75, 0.05, 2.0)
    bgc_class_weight["PKS-NRP_Hybrids"] = (0.0, 0.78, 0.22, 1.0)
    bgc_class_weight["Others"] = (0.01, 0.97, 0.02, 4.0)
    bgc_class_weight["Custom"] = (0.0, 1.0, 0.0, 4.0)
    # define which classes will be analyzed (if in the options_classify mode)
    valid_classes = set()
    for key in bgc_class_weight:
        valid_classes.add(key.lower())
    user_banned_classes = set([a.strip().lower() for a in options.banned_classes])
    valid_classes = valid_classes - user_banned_classes
    bgc_class_weight["mix"] = (0.2, 0.75, 0.05, 2.0)  # default when not separating in classes
    BGC_classes = defaultdict(list)
    # mix class will always be the last element of the tuple
    bgcClassNames = tuple(sorted(list(bgc_class_weight)) + ["mix"])
    assert bgcClassNames[-1] == 'mix'

    bgcClassName2idx = dict(zip(bgcClassNames, range(len(bgcClassNames))))

    AlignedDomainSequences = {}  # Key: specific domain sequence label. Item: aligned sequence
    DomainList = {}  # Key: BGC. Item: ordered list of domains

    # to avoid multiple alignment if there's only 1 seq. representing a particular domain
    sequences_per_domain = {}
    ### Step 2: Run hmmscan
    print("\nPredicting domains using hmmscan")

    baseNames = set(clusters)

    # All available fasta files (could be more than it should if reusing output folder)
    allFastaFiles = set(glob(os.path.join(bgc_fasta_folder, "*.fasta")))

    # fastaFiles: all the fasta files that should be there
    # (i.e. correspond to the input files)
    fastaFiles = set()
    for name in baseNames:
        fastaFiles.add(os.path.join(bgc_fasta_folder, name + ".fasta"))

    # fastaBases: the actual fasta files we have that correspond to the input
    fastaBases = allFastaFiles.intersection(fastaFiles)

    # Verify that all input files had their fasta sequences extracted
    if len(fastaFiles - fastaBases) > 0:
        sys.exit("Error! The following files did NOT have their fasta sequences extracted: " + ", ".join(
            fastaFiles - fastaBases))

    # Make a list of all fasta files that need to be processed
    # (i.e., they don't yet have a corresponding .domtable)
    if options.force_hmmscan:
        # process all files, regardless of whether they already existed
        task_set = fastaFiles
        print(" Forcing domain prediction on ALL fasta files (--force_hmmscan)")
    else:
        # find already processed files
        alreadyDone = set()
        for fasta in fastaFiles:
            outputbase = ".".join(fasta.split(os.sep)[-1].split(".")[:-1])
            outputfile = os.path.join(domtable_folder, outputbase + '.domtable')
            if os.path.isfile(outputfile) and os.path.getsize(outputfile) > 0:
                alreadyDone.add(fasta)

        if len(fastaFiles - alreadyDone) == 0:
            print(" All fasta files had already been processed")
        elif len(alreadyDone) > 0:
            if len(fastaFiles - alreadyDone) < 20:
                print " Warning! The following NEW fasta file(s) will be processed: %s" % ", ".join(
                    ".".join(x.split(os.sep)[-1].split(".")[:-1]) for x in fastaFiles - alreadyDone)
            else:
                print(" Warning: " + str(len(fastaFiles - alreadyDone)) + " NEW fasta files will be processed")
        else:
            print(" Predicting domains for " + str(len(fastaFiles)) + " fasta files")

        task_set = fastaFiles - alreadyDone

    pool = Pool(cores, maxtasksperchild=1)
    for fastaFile in task_set:
        pool.apply_async(runHmmScan, args=(fastaFile, pfam_dir, domtable_folder, verbose))
    pool.close()
    pool.join()

    print " Finished generating domtable files."

    ### Step 3: Parse hmmscan domtable results and generate pfs and pfd files
    print("\nParsing hmmscan domtable files")

    # All available domtable files
    allDomtableFiles = set(glob(os.path.join(domtable_folder, "*.domtable")))

    # domtableFiles: all domtable files corresponding to the input files
    domtableFiles = set()
    for name in baseNames:
        domtableFiles.add(os.path.join(domtable_folder, name + ".domtable"))

    # domtableBases: the actual set of input files with coresponding domtable files
    domtableBases = allDomtableFiles.intersection(domtableFiles)

    # Verify that all input files have a corresponding domtable file
    if len(domtableFiles - domtableBases) > 0:
        sys.exit("Error! The following files did NOT have their domains predicted: " + ", ".join(
            domtableFiles - domtableBases))

    # find already processed files (assuming that if the pfd file exists, the pfs should too)
    alreadyDone = set()
    if not options.force_hmmscan:
        for domtable in domtableFiles:
            outputbase = ".".join(domtable.split(os.sep)[-1].split(".")[:-1])
            outputfile = os.path.join(pfd_folder, outputbase + '.pfd')
            if os.path.isfile(outputfile) and os.path.getsize(outputfile) > 0:
                alreadyDone.add(domtable)

    if len(domtableFiles - alreadyDone) == 0:  # Re-run
        print(" All domtable files had already been processed")
    elif len(alreadyDone) > 0:  # Incomplete run
        if len(domtableFiles - alreadyDone) < 20:
            print " Warning! The following domtable files had not been processed: %s" % ", ".join(
                ".".join(x.split(os.sep)[-1].split(".")[:-1]) for x in domtableFiles - alreadyDone)
        else:
            print(" Warning: " + str(len(domtableFiles - alreadyDone)) + " domtable files will be processed")
    else:  # First run
        print(" Processing " + str(len(domtableFiles)) + " domtable files")

    # If using the multiprocessing version and outputbase doesn't have any
    #  predicted domains, it's not as easy to remove if from the analysis
    #  (probably because parseHmmScan only has a copy of clusters et al?)
    # Using serialized version for now. Probably doesn't have too bad an impact
    # pool = Pool(cores,maxtasksperchild=32)
    for domtableFile in domtableFiles - alreadyDone:
        parseHmmScan(domtableFile, pfd_folder, pfs_folder, options.domain_overlap_cutoff)
        # pool.apply_async(parseHmmScan, args=(domtableFile,output_folder,options.domain_overlap_cutoff))
    # pool.close()
    # pool.join()

    # If number of pfd files did not change, no new sequences were added to the
    #  domain fastas and we could try to resume the multiple alignment phase
    # baseNames have been pruned of BGCs with no domains that might've been added temporarily
    try_MA_resume = False
    if len(baseNames - set(pfd.split(os.sep)[-1][:-9] for pfd in alreadyDone)) == 0:
        try_MA_resume = True
    else:
        # new sequences will be added to the domain fasta files. Clean domains folder
        # We could try to make it so it's not necessary to re-calculate every alignment,
        #  either by expanding previous alignment files or at the very least,
        #  re-aligning only the domain files of the newly added BGCs
        print(" New domain sequences to be added; cleaning domains folder")
        for thing in os.listdir(domains_folder):
            os.remove(os.path.join(domains_folder, thing))

    print " Finished generating generating pfs and pfd files."

    ### Step 4: Parse the pfs, pfd files to generate BGC dictionary, clusters, and clusters per sample objects
    print("\nProcessing domains sequence files")

    # All available pfd files
    allPfdFiles = set(glob(os.path.join(pfd_folder, "*.pfd")))

    # pfdFiles: all pfd files corresponding to the input files
    # (some input files could've been removed due to not having predicted domains)
    pfdFiles = set()
    for name in baseNames:
        pfdFiles.add(os.path.join(pfd_folder, name + ".pfd"))

    # pfdBases: the actual set of input files that have pfd files
    pfdBases = allPfdFiles.intersection(pfdFiles)

    # verify previous step.
    # All BGCs without predicted domains should no longer be in baseNames
    if len(pfdFiles - pfdBases) > 0:
        sys.exit(
            "Error! The following files did NOT have their domtable files processed: " + ", ".join(pfdFiles - pfdBases))

    filtered_matrix = []
    if options.skip_ma:
        print(" Running with skip_ma parameter: Assuming that the domains folder has all the fasta files")
        try:
            with open(os.path.join(output_folder + "BGCs.dict"), "r") as BGC_file:
                BGCs = pickle.load(BGC_file)
                BGC_file.close()
        except IOError:
            sys.exit("BGCs file not found...")
    else:
        print(" Adding sequences to corresponding domains file")

        for outputbase in baseNames:
            if verbose:
                print("   Processing: " + outputbase)

            pfdFile = os.path.join(pfd_folder, outputbase + ".pfd")
            filtered_matrix = [map(lambda x: x.strip(), line.split('\t')) for line in open(pfdFile)]

            # save each domain sequence from a single BGC in its corresponding file
            fasta_file = os.path.join(bgc_fasta_folder, outputbase + ".fasta")

            # only create domain fasta if the pfd content is different from original and
            #  domains folder has been emptied. Else, if trying to resume alignment phase,
            #  domain fasta files will contain duplicate sequence labels
            if not try_MA_resume:
                with open(fasta_file, "r") as fasta_file_handle:
                    fasta_dict = fasta_parser(fasta_file_handle)  # all fasta info from a BGC
                save_domain_seqs(filtered_matrix, fasta_dict, domains_folder, outputbase)

            BGCs[outputbase] = BGC_dic_gen(filtered_matrix)

            del filtered_matrix[:]

        # store processed BGCs dictionary for future re-runs
        if not os.path.isfile(output_folder + "BGCs.dict"):
            with open(os.path.join(output_folder, "BGCs.dict"), "w") as BGC_file:
                pickle.dump(BGCs, BGC_file)
                BGC_file.close()

    # Get the ordered list of domains
    print(" Reading the ordered list of domains from the pfs files")
    for outputbase in baseNames:
        pfsfile = os.path.join(pfs_folder, outputbase + ".pfs")
        if os.path.isfile(pfsfile):
            DomainList[outputbase] = get_domain_list(pfsfile)
        else:
            sys.exit(" Error: could not open " + outputbase + ".pfs")

    ### Step 5: Create SVG figures
    print(" Creating arrower-like figures for each BGC")

    # verify if there are figures already generated

    # All available SVG files
    availableSVGs = set()
    for svg in glob(os.path.join(svg_folder, "*.svg")):
        (root, ext) = os.path.splitext(svg)
        availableSVGs.add(root.split(os.sep)[-1])

    # Which files actually need to be generated
    working_set = baseNames - availableSVGs

    if len(working_set) > 0:
        color_genes = read_color_genes_file()
        color_domains = read_color_domains_file()
        pfam_domain_categories = read_pfam_domain_categories()

        print("  Parsing hmm file for domain names")
        pfam_info = {}
        with open(os.path.join(pfam_dir, "Pfam-A.hmm"), "r") as pfam:
            putindict = False
            # assuming that the order of the information never changes
            for line in pfam:
                if line[:4] == "NAME":
                    name = line.strip()[6:]
                if line[:3] == "ACC":
                    acc = line.strip()[6:].split(".")[0]
                if line[:4] == "DESC":
                    desc = line.strip()[6:]
                    putindict = True

                if putindict:
                    putindict = False
                    pfam_info[acc] = (name, desc)
        print("    Done")

        # This must be done serially, because if a color for a gene/domain
        # is not found, the text files with colors need to be updated
        print("  Reading BGC information and writing SVG")
        for bgc in working_set:
            SVG(False, os.path.join(svg_folder, bgc + ".svg"), genbankDict[bgc][0],
                os.path.join(pfd_folder, bgc + ".pfd"), True, color_genes, color_domains, pfam_domain_categories,
                pfam_info, bgc_info[bgc].records, bgc_info[bgc].max_width)

        color_genes.clear()
        color_domains.clear()
        pfam_domain_categories.clear()
    elif len(working_set) == 0:
        print("  All SVG from the input files seem to be in the SVG folder")

    availableSVGs.clear()
    print(" Finished creating figures")

    print("\n\n   - - Calculating distance matrix - -")

    # Do multiple alignments if needed
    if not options.skip_ma:
        print("Performing multiple alignment of domain sequences")

        # obtain all fasta files with domain sequences
        fasta_domains = set(glob(os.path.join(domains_folder, "*.fasta")))

        # compare with .algn set of files. Maybe resuming is possible if
        # no new sequences were added
        if try_MA_resume:
            temp_aligned = set(glob(os.path.join(domains_folder, "*.algn")))

            if len(temp_aligned) > 0:
                print(" Found domain fasta files without corresponding alignments")

                for a in temp_aligned:
                    if os.path.getsize(a) > 0:
                        fasta_domains.remove(a[:-5] + ".fasta")

            temp_aligned.clear()

        # Try to further reduce the set of domain fastas that need alignment
        sequence_tag_list = set()
        header_list = []
        fasta_domains_temp = fasta_domains.copy()
        for domain_file in fasta_domains_temp:
            domain_name = ".".join(domain_file.split(os.sep)[-1].split(".")[:-1])

            # fill fasta_dict...
            with open(domain_file, "r") as fasta_handle:
                header_list = get_fasta_keys(fasta_handle)

            # Get the BGC name from the sequence tag. The form of the tag is:
            # >BGCXXXXXXX_BGCXXXXXXX_ORF25:gid...
            sequence_tag_list = set(s.split("_ORF")[0] for s in header_list)

            # ...to find out how many sequences do we actually have
            #if len(sequence_tag_list) == 1:
                # avoid multiple alignment if the domains all belong to the same BGC
            #    fasta_domains.remove(domain_file)
            #    if verbose:
            #        print(" Skipping Multiple Alignment for " + domain_name + " (appears only in one BGC)")

        sequence_tag_list.clear()
        del header_list[:]

        fasta_domains_temp.clear()

        # Do the multiple alignment
        if len(fasta_domains) > 0:
            print("\n Using hmmalign")
            launch_hmmalign(cores, fasta_domains)

            # verify all tasks were completed by checking existance of alignment file
            for domain in fasta_domains:
                if not os.path.isfile(domain[:-6] + ".algn"):
                    print("   WARNING, " + domain[:-6] + ".algn could not be found (possible issue with aligner).")

        else:
            print(" No domain fasta files found to align")

    # If there's something to analyze, load the aligned sequences
    if options_samples or options_all:
        print(" Trying to read domain alignments (*.algn files)")
        aligned_files_list = glob(os.path.join(domains_folder, "*.algn"))
        if len(aligned_files_list) == 0:
            sys.exit(
                "No aligned sequences found in the domain folder (run without the --skip_ma parameter or point to the correct output folder)")
        for aligned_file in aligned_files_list:
            with open(aligned_file, "r") as aligned_file_handle:
                fasta_dict = fasta_parser(aligned_file_handle)
                for header in fasta_dict:
                    AlignedDomainSequences[header] = fasta_dict[header]

    # pickle all needed files:
    if not os.path.isfile(output_folder + "DomainList.p"):
        pickle.dump(DomainList, open(output_folder + "DomainList.p", "w"))
    if not os.path.isfile(output_folder + "AlignedDomainSequences.p"):
        pickle.dump(AlignedDomainSequences, open(output_folder + "AlignedDomainSequences.p", "w"))
    if not os.path.isfile(output_folder + "bgc_info.p"):
        pickle.dump(bgc_info, open(output_folder + 'bgc_info.p', "w"))

