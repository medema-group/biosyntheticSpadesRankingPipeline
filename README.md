# BiosyntheticSPAdes putative clusters ranking pipeline

## Description
Ranking pipeline to score putative BGCs generated by the biosyntheticSPAdes assembler against a database of BGCs.
To use it you should provide the path to the "bgc_statistics.txt" file and a database of BGCs to rank the putative BGCs [as MIBIG or antismash-DB].

### Warning
We recommend you further validate the BGCs produced by the ranking pipeline with wetlab experiments. This is a reference based method and is not indicated for completely novel BGCs. 

## Requirements

*   The [HMMER suite](http://hmmer.org/) (3.1b2+)
*   The (processed) Pfam database. For this, download the latest `Pfam-A.hmm.gz` file from the [Pfam website](ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release), uncompress it and process it using the `hmmpress` command.
*   [biopython](https://biopython.org/)
*   numpy
*   scipy
*   [Antismash](https://github.com/antismash)

## Preprocess BGC database 
The preprocessed antismash-DB database is available for download through **sftp**:

server: sftp.ab.wur.nl
login: bioinfoshare03
passwd: H9U66Vn&

Once connected, navigate to the data folder to get the preprocessed_antismash-db.tar.gz file

## Example 

```bash
python biosyntheticSPAdes_ranking_pipeline.py -asdb processed_antismashdb/ -bss assembly/bgc_statistics.txt -i assembly/ 
```
