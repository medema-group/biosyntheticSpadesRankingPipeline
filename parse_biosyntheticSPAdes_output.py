import os
import re

def findCandidates(bgc_statistics_path, path):
    print (path)
    print (bgc_statistics_path)
    if os.path.isfile(bgc_statistics_path):
        print ('bgc_statistics file found for {} genome'.format(bgc_statistics_path))
        bgc_stats = open(bgc_statistics_path).read()
        for bgc in re.split('BGC subgraph', bgc_stats)[1:]:
            bgc_id = int(bgc.strip(' ').split('\n')[0])
            bgc = bgc.split('\n')
            if bgc[1]:
                if 'BGC candidate 2' in bgc:
                    print 'Biosynthetic-SPAdes produced multiple putative BGCs structures, \
                    writing it to fasta file'
                    #domain_count = int(bgc[1].split(' ')[-1])
                    #strong_edges = int(bgc[2].split(' ')[-1])
                    #weak_edges = int(bgc[3].split(' ')[-1])
                    writeCandidates(bgc_id, bgc_statistics_path, path)

    else:
        print "bgc_statistics file not found"
        writeCandidates('', bgc_statistics_path, path)

def writeCandidates(bgc_id, bgc_statistics_path, path):
    try:
        print ("opening {}/ordering.fasta file".format('/'.join(bgc_statistics_path.split('/')[:-1])))
        fasta = open('/'.join(bgc_statistics_path.split('/')[:-1]) + "/orderings.fasta").read().split('>')[1:]
    except IOError:
        print ("opening {}/gene_clusters.fasta file".format('/'.join(bgc_statistics_path.split('/')[:-1])))
        fasta = open('/'.join(bgc_statistics_path.split('/')[:-1]) + "/gene_clusters.fasta").read().split('>')[1:]

    for f in fasta:
        header = f.split('\n')[0]
        h = header.split('_')[5] + '_' + header.split('_')[7]
        header = '_'.join(f.split('\n')[0].split('_')[5:])
        #print (header.split('_')[0], int(bgc_id), int(header.split('_')[0]) == int(bgc_id), type(int(header.split('_')[0])), type(int(bgc_id)))
        if not os.path.isdir('{}/candidates'.format(path)):
            os.makedirs('{}/candidates'.format(path))
        if not os.path.isfile('{}/candidates/BGC_{}.fasta'.format(path, h)):
            if int(header.split('_')[0]) == int(bgc_id):
                outtxt = '>' + header + '\n' + '\n'.join(f.split('\n')[1:])
                outfile = open('{}/candidates/BGC_{}.fasta'.format(path, h), 'w')
                outfile.write(outtxt)
                outfile.close()
