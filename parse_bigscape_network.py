#!/usr/bin/env python

"""
PI: Marnix Medema               marnix.medema@wur.nl

Developers:
Jorge Navarro                   jorge.navarromunoz@wur.nl
Emmanuel (Emzo) de los Santos   E.De-Los-Santos@warwick.ac.uk
Marley Yeong                    marleyyeong@live.nl
Vittorio Tracanna               vittorio.tracanna@wur.nl


Dependencies: hmmer, biopython, (mafft), munkres.py

See more info on
https://git.wageningenur.nl/medema-group/biosyntheticSpadesRankingPipeline

# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.
"""

import pandas as pd
import pickle

def load_network(network):
    '''
    :param network: BiG-SCAPE network file
    :return: parsed network file in a multi-indexed pandas DataFrame
    '''
    nw = pd.read_csv(network, sep='\t')
    arrays = [nw['Clustername 2'], nw['Clustername 1']]
    tuples = list(zip(*arrays))
    index = pd.MultiIndex.from_tuples(tuples, names=['Clustername 2', 'Clustername 1'])
    nw = nw.set_index(index)
    nw = nw[nw.columns.values[2:]]
    return nw

def get_DSS(nw, BGCs_group_id):
    '''
    :param nw: pandas DataFrame that contains the BiG-SCAPE network 
    :param gbk_folder: path [folder] to the genbank files
    :return group DSS: {BGC_subgraph_id: {putative_BGC_id : [best_DSS_hit, DSS_index]...}
    '''
    group_DSS = {}
    for putative_BGC, sub_nw in nw.groupby(level=1):
        putative_BGC_group = BGCs_group_id[putative_BGC]
        if putative_BGC_group in group_DSS:
            group_DSS[putative_BGC_group][sub_nw['DSS index'].idxmax(0)[1]] = \
                [sub_nw['DSS index'].idxmax(0)[0], max(sub_nw['DSS index'])]
        else:
            group_DSS[putative_BGC_group] = {}
            group_DSS[putative_BGC_group][sub_nw['DSS index'].idxmax(0)[1]] = \
                [sub_nw['DSS index'].idxmax(0)[0], max(sub_nw['DSS index'])]
    return group_DSS

def find_reference(DSS):
    '''
    :param DSS: parsed DSS from nw
    :return: reference, dictionary {BGC_group: referenceBGC}
    '''
    reference = {}
    for BGC_group in DSS.keys():
        if BGC_group not in reference:
            reference[BGC_group] = ''
            DSS_value = 0
        for k in DSS[BGC_group].keys():
            if DSS[BGC_group][k][1] > DSS_value:
                reference[BGC_group] = DSS[BGC_group][k][0];   DSS_value = DSS[BGC_group][k][1]
    return reference

def rank_putative_BGC(ref, nw, DSS):
    '''
    :param ref: dictionary with reference BGC from db for each group in query
    :param nw: network file
    :param DSS: 
    :return: 
    '''
    ranks = {}
    pickle.dump(nw, open('temp.p','wb'))
    for BGC_group in DSS.keys():
        ranks[BGC_group] = []
        for k in DSS[BGC_group].keys():
            try:
                ranks[BGC_group].append([k, nw.loc[ref[BGC_group]].loc[k]['Adjacency index'], \
                                         nw.loc[ref[BGC_group]].loc[k]['Raw distance'], ref[BGC_group]])
            except KeyError:
                continue
            ranks[BGC_group]=sorted(ranks[BGC_group], key=lambda x: (x[1], 1-x[2]), reverse=True)
    out_txt = 'BGC group\tReference BGC\tBGC id\tRank\tAdjacency score\tConfidence\n'
    # header
    for k,v in ranks.items():
        r = 1
        for i in range(len(v)):
            if v[i][2] != v[i-1][2] and i != 0:
                r += 1
            out_txt+= str(k) + '\t' + v[i][3] + '\t' + v[i][0] + '\t' + str(r) \
                      + '\t' + str(v[i][1]) + '\t' + str(1-v[i][2]) + '\n'
        out_txt += '------------------------------------------------------------------------\n'
    return out_txt

